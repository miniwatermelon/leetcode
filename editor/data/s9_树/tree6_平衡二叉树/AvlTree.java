package s9_树.tree6_平衡二叉树;

/**
 * Created by 西瓜瓜
 * Date :2022/2/26
 * Description :
 * Version :1.0
 *
 * 平衡二叉树：
 * 1.也叫平衡二叉搜索树，又被称为AVL树，可以保证查询效率较高
 * 2.特点：它是一颗空树或它的左右两个子树的高度差的绝对值不超过1，并且左右两个子树都是一颗平衡二叉树
 * 常用实现方法：红黑树，AVL，替罪羊树，Treap，伸展树等
 *
 * 如何处理左旋转：
 * 1.创建一个新节点，值等于当前根节点的值，
 * 2.把新节点的左子树设置为当前根节点的左子树
 * 3.把新节点的右子树设置为当前根节点的右子树的左子树
 * 4.把当前根节点的值替换为右子节点的值
 * 5.把当前根节点的右子树设置成右子树的右子树
 * 6.把当前根节点的左子树设置为新节点
 *
 * 如何处理右旋转：
 * 1.创建一个新节点，值等于当前根节点的值，
 * 2.把新节点的右子树设置为当前根节点的右子树
 * 3.把新节点的左子树设置为当前根节点的左子树的右子树
 * 4.把当前根节点的值替换为左子节点的值
 * 5.把当前根节点的左子树设置成左子树的左子树
 * 6.把当前根节点的右子树设置为新节点
 */
public class AvlTree {

    private AvlTreeNode root;

    public void add(AvlTreeNode node) {
        if(root == null){
            root = node;
            return;
        }

        root.add(node);
    }

    public void infixOrder() {
        if(root != null) {
            root.infixOrder();
        }
    }

    public AvlTreeNode search(int value) {
        if(root != null) {
            return root.search(value);
        }
        return null;
    }

    public AvlTreeNode searchParent(int value) {
        if(root != null) {
            return root.searchParent(value);
        }
        return null;
    }

    public void delNode(int val) {
        if(root == null) return;

        AvlTreeNode targetNode = search(val);
        if(targetNode == null) return;

        if(root.leftNode == null && root.rightNode == null) {
            // 说明只有根节点
            root = null;
            return;
        }

        AvlTreeNode parentNode = searchParent(val);
        // 说明是叶子节点
        if(targetNode.leftNode == null && targetNode.rightNode == null) {
            if(parentNode.leftNode != null && parentNode.leftNode.value == val) {
                parentNode.leftNode = null;
                return;
            }
            if(parentNode.rightNode != null && parentNode.rightNode.value == val) {
                parentNode.rightNode = null;
                return;
            }
        }

        // 说明待删除节点还有全子树
        if(targetNode.leftNode != null && targetNode.rightNode != null) {
            targetNode.value = delRightTreeMin(targetNode.rightNode);
            return;
        }

        // 说明待删除节点还有左子树
        if(targetNode.leftNode != null) {
            if(parentNode == null) {
                root = targetNode.leftNode;
                return;
            }

            if(parentNode.leftNode != null && parentNode.leftNode.value == val) {
                parentNode.leftNode = targetNode.leftNode;
                return;
            }
            if(parentNode.rightNode != null && parentNode.rightNode.value == val) {
                parentNode.rightNode = targetNode.leftNode;
                return;
            }

        }

        // 说明待删除节点还有右子树
        if(targetNode.rightNode != null) {
            if(parentNode == null) {
                root = targetNode.rightNode;
                return;
            }

            if(parentNode.leftNode != null && parentNode.leftNode.value == val) {
                parentNode.leftNode = targetNode.rightNode;
                return;
            }
            if(parentNode.rightNode != null && parentNode.rightNode.value == val) {
                parentNode.rightNode = targetNode.rightNode;
            }

        }
    }

    public int delRightTreeMin(AvlTreeNode node) {
        AvlTreeNode temp = node;

        // 循环左节点找到最小值
        while(temp.leftNode != null) {
            temp = temp.leftNode;
        }
        // 这时temp指向最小节点
        delNode(temp.value);
        return temp.value;
    }

    public static void main(String[] args) {
        int[] arr = {4,3,6,5,7,8};

        AvlTree avlTree = new AvlTree();
        for (int value : arr) {
            avlTree.add(new AvlTreeNode(value));
        }

        avlTree.infixOrder();

        System.out.println(avlTree.root.height());
        System.out.println(avlTree.root.leftHeight());
        System.out.println(avlTree.root.rightHeight());
    }
}

class AvlTreeNode {
    int value;

    AvlTreeNode leftNode;
    AvlTreeNode rightNode;

    public AvlTreeNode(int value) {
        this.value = value;
    }

    /**
     * 返回当前节点的高度
     * @return
     */
    public int height() {
        return Math.max(this.leftNode == null ? 0 : this.leftNode.height(), this.rightNode == null ? 0 : this.rightNode.height()) + 1;
    }

    /**
     * 返回当前节点左子树的高度
     * @return
     */
    public int leftHeight() {
        if(leftNode == null) return 0;

        return leftNode.height();
    }

    /**
     * 返回当前节点右子树的高度
     * @return
     */
    public int rightHeight() {
        if(rightNode == null) return 0;

        return rightNode.height();
    }

    /**
     * 左旋转
     */
    public void leftRotate() {
        // 1.创建一个新节点，值等于当前根节点的值，
        AvlTreeNode node = new AvlTreeNode(this.value);
        // 2.把新节点的左子树设置为当前根节点的左子树
        node.leftNode = this.leftNode;
        // 3.把新节点的右子树设置为当前根节点的右子树的左子树
        node.rightNode = this.rightNode.leftNode;
        // 4.把当前根节点的值替换为右子树的值
        this.value = this.rightNode.value;
        // 5.把当前根节点的右子树设置成右子树的右子树
        this.rightNode = this.rightNode.rightNode;
        // 6.把当前根节点的左子树设置为新节点
        this.leftNode = node;

    }


    /**
     * 右旋转
     */
    public void rightRotate() {
        // 1.创建一个新节点，值等于当前根节点的值，
        AvlTreeNode node = new AvlTreeNode(this.value);
        // 2.把新节点的右子树设置为当前根节点的右子树
        node.rightNode = this.rightNode;
        // 3.把新节点的左子树设置为当前根节点的左子树的右子树
        node.leftNode = this.leftNode.rightNode;
        // 4.把当前根节点的值替换为左子节点的值
        this.value = this.leftNode.value;
        // 5.把当前根节点的左子树设置成左子树的左子树
        this.leftNode = this.leftNode.leftNode;
        // 6.把当前根节点的右子树设置为新节点
        this.rightNode = node;

    }

    public void add(AvlTreeNode node) {
        if(node == null) return;

        // 待添加的节点值小于当前节点值
        if(node.value < this.value) {
            if(this.leftNode == null) {
                // 当前节点的左子树为空，则挂在到当前节点的左子节点
                this.leftNode = node;
            } else {
                // 递归向左子树添加
                this.leftNode.add(node);
            }
        } else {
            if(this.rightNode == null) {
                // 当前节点的右子树为空，则挂在到当前节点的右子节点
                this.rightNode = node;
            } else {
                // 递归向右子树添加
                this.rightNode.add(node);
            }
        }


        // 当添加完一个节点后，如果 右子树的高度-左子树的高度 > 1，则左旋转
        if(rightHeight() - leftHeight() > 1) {
            // 如果它的右子树的左子树高度大于它的右子树的右子树高度
            if(rightNode != null && rightNode.rightHeight() < rightNode.leftHeight()) {
                // 先对当前节点的右子树进行右旋转
                rightNode.rightRotate();
            }
            // 对当前节点进行左旋转
            leftRotate();
            return;
        }

        // 当添加完一个节点后，如果 左子树的高度-右子树的高度 > 1，则右旋转
        if(leftHeight() - rightHeight() > 1) {
            // 如果它的左子树的右子树高度大于它的左子树的左子树高度
            if(leftNode != null && leftNode.rightHeight() > leftNode.leftHeight()) {
                // 先对当前节点的左子树进行左旋转
                leftNode.leftRotate();
            }
            // 对当前节点进行右旋转
            rightRotate();
        }
    }

    /**
     * 中序遍历
     */
    public void infixOrder() {
        if(this.leftNode != null) {
            this.leftNode.infixOrder();
        }
        System.out.println(this);
        if (this.rightNode != null) {
            this.rightNode.infixOrder();
        }
    }

    /**
     * 查找要删除的节点
     * @param value 待删除节点的值
     * @return 返回当前节点
     */
    public AvlTreeNode search(int value) {
        if(value == this.value) return this;

        if(value < this.value) {
            if(this.leftNode == null) return null;
            return this.leftNode.search(value);
        } else {
            if(this.rightNode == null) return null;
            return this.rightNode.search(value);
        }
    }

    /**
     * 查找要删除节点的父节点
     * @param value 待删除节点的值
     * @return 返回父节点
     */
    public AvlTreeNode searchParent(int value) {
        if((this.leftNode != null && this.leftNode.value == value) ||
                (this.rightNode != null && this.rightNode.value == value)) {
            return this;
        }

        if(value < this.value && this.leftNode != null) {
            return this.leftNode.searchParent(value);
        }

        if(value >= this.value && this.rightNode != null) {
            return this.rightNode.searchParent(value);
        }

        return null;
    }




    @Override
    public String toString() {
        return "AvlTreeNode{" +
                "value=" + value +
                '}';
    }
}


