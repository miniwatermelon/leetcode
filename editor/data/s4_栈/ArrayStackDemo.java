package s4_栈;

/**
 * @author wisdomelon
 * @date 2020/7/27 0027
 * @project data_study
 * @jdk 1.8
 * 数组模拟栈
 */
public class ArrayStackDemo {

    public static void main(String[] args) {
        ArrayStack arrayStack = new ArrayStack(10);

        //int pop = arrayStack.pop();
        //System.out.println(pop);

        arrayStack.push(1);
        arrayStack.push(2);
        arrayStack.push(3);
        arrayStack.push(4);
        arrayStack.push(5);
        arrayStack.push(6);
        arrayStack.push(7);
        arrayStack.push(8);
        arrayStack.push(9);
        arrayStack.push(10);

        int pop1 = arrayStack.pop();
        System.out.println(pop1);

        arrayStack.list();

    }

}

class ArrayStack {
    /** 最大大小 */
    private int maxSize;
    /** 数据放在数组中 */
    private int[] stack;
    /** 栈顶 初始化为 -1 */
    private int top = -1;

    public ArrayStack(int maxSize) {
        this.maxSize = maxSize;
        this.stack = new int[maxSize];
    }

    /**
     * 栈满
     * @return
     */
    public boolean isFull() {
        return top == (maxSize-1);
    }

    /**
     * 栈空
     * @return
     */
    public boolean isEmpty() {
        return top == -1;
    }

    /**
     * 入栈
     * @param data
     */
    public void push(int data) {

        if(isFull()) {
            return;
        }

        this.stack[++top] = data;
    }

    /**
     * 出栈
     */
    public int pop() {
        if(isEmpty()) {
            throw new RuntimeException("栈为空！");
        }

        return this.stack[top--];
    }

    /**
     * 遍历
     */
    public void list() {
        if(isEmpty()) {
            return;
        }

        for (int i=top; i>-1; i--) {
            System.out.println(this.stack[i]);
        }
    }
}
