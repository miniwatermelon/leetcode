package s6_排序算法.sort2_选择排序;

import s6_排序算法.BaseSort;

import java.util.Arrays;

/**
 * @author wisdomelon
 * @date 2020/7/28 0028
 * @project data_study
 * @jdk 1.8
 * 简单选择排序
 */
public class SimpleChooseSort extends BaseSort {

    public static void main(String[] args) {
        int[] arr = data();
        System.out.println(Arrays.toString(arr));
        new SimpleChooseSort().asc(arr);
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public void asc(int[] arr) {
        System.out.println("SimpleChooseSort-------------------------------");

        long s = System.currentTimeMillis();
        for(int i=0; i<arr.length-1; i++) {
            // 假定当前数是最小数
            int res = arr[i];
            int index = i;

            for(int j=i+1; j<arr.length; j++) {
                if(res > arr[j]) {
                    // 假定的最小数比循环的数大，则替换最小数和下标
                    res = arr[j];
                    index = j;
                }
            }
            // 将最小数的位置与当前数的位置进行交换
            if(index != i) {
                arr[index] = arr[i];
                arr[i] = res;
            }
        }

        long e = System.currentTimeMillis();
        System.out.println("SimpleChooseSort: " + (e-s) + "ms");
    }
}
