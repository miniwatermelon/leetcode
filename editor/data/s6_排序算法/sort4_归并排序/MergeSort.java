package s6_排序算法.sort4_归并排序;

import s6_排序算法.BaseSort;

import java.util.Arrays;

/**
 * @author wisdomelon
 * @date 2020/7/29 0029
 * @project data_study
 * @jdk 1.8
 * 归并排序
 */
public class MergeSort extends BaseSort {

    public static void main(String[] args) {
        //int[] arr = normal();
        int[] arr = {8,4,5,7,1,3,6,2};
        System.out.println(Arrays.toString(arr));
        new MergeSort().asc(arr);
        System.out.println(Arrays.toString(arr));
    }


    @Override
    public void asc(int[] arr) {

        System.out.println("MergeSort-------------------------------");
        long s = System.currentTimeMillis();

        mergeSort(arr, 0, arr.length-1, new int[arr.length]);

        long e = System.currentTimeMillis();
        System.out.println("MergeSort: " + (e-s) + "ms");

    }

    /**
     * 归并排序
     * @param arr
     * @param left 左边有序序列的初始索引
     * @param right 右边索引
     * @param temp 中转数组
     * @return
     */
    private void mergeSort(int[] arr, int left, int right, int[] temp) {

        if(left < right) {
            int mid = (left + right) / 2;
            // 向左递归分解，这里可以分解到最小单元
            mergeSort(arr, left, mid, temp);
            // 向右递归分解，这里可以分解到最小单元
            mergeSort(arr, mid + 1, right, temp);
            // 合并
            merge(arr, left, mid, right, temp);
        }

    }

    /**
     * 归并排序
     * @param arr
     * @param left 左边有序序列的初始索引
     * @param mid 中间索引
     * @param right 右边索引
     * @param temp 中转数组
     * @return
     */
    private void merge(int[] arr, int left, int mid, int right, int[] temp) {

        // 初始化左数组移动的初始索引
        int i=left;
        // 初始化右数组移动的初始索引
        int j=mid+1;
        // 指向中转数组的当前索引
        int t = 0;

        // 把左右两边已排好序的数据按照规则填充到temp数组中，直到某一边的数据都处理完毕
        while(i <= mid && j <= right) {
            if(arr[i] <= arr[j]) {
                temp[t] = arr[i];
                i++;
                t++;
            } else {
                temp[t] = arr[j];
                j++;
                t++;
            }
        }

        // 把有剩余数据一边的数据依次全部填充到temp数组中
        while(i <= mid) {
            temp[t] = arr[i];
            i++;
            t++;
        }

        while(j <= right) {
            temp[t] = arr[j];
            j++;
            t++;
        }

        // 将temp数组的元素拷贝到arr
        t = 0;
        int l = left;
        while(l <= right) {
            arr[l] = temp[t];
            l++;
            t++;
        }
    }
}
