package s6_排序算法.sort1_交换排序;

import s6_排序算法.BaseSort;

import java.util.Arrays;

/**
 * @author wisdomelon
 * @date 2020/7/28 0028
 * @project data_study
 * @jdk 1.8
 * 冒泡排序 时间复杂度 O(n²)
 */
public class BubbleSort extends BaseSort {

    public static void main(String[] args) {
        int[] arr = data();
        System.out.println(Arrays.toString(arr));
        new BubbleSort().asc(arr);
        System.out.println(Arrays.toString(arr));
    }


    @Override
    public void asc(int[] arr) {

        System.out.println("BubbleSort-------------------------------");
        long s = System.currentTimeMillis();
        for(int i=0; i<arr.length-1; i++) {
            boolean flag = true;
            for(int j=0; j<arr.length-1-i; j++) {
                // 相邻两个数左边比右边的大，则发生交换
                if(arr[j] > arr[j + 1]) {
                    int temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                    flag = false;
                }
            }
            // 没有交换过，说明已排序完成，则退出循环
            if(flag) {
                break;
            }
        }
        long e = System.currentTimeMillis();
        System.out.println("BubbleSort: " + (e-s) + "ms");
    }
}
