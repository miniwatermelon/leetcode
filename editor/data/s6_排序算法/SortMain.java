package s6_排序算法;

import s6_排序算法.sort1_交换排序.BubbleSort;
import s6_排序算法.sort1_交换排序.QuickSort;
import s6_排序算法.sort2_选择排序.SimpleChooseSort;
import s6_排序算法.sort3_插入排序.InsertionSort;
import s6_排序算法.sort3_插入排序.ShellSort;
import s6_排序算法.sort4_归并排序.MergeSort;
import s6_排序算法.sort5_基数排序.RadixSort;
import s6_排序算法.sort6_堆排序.HeapSort;
import s9_树.tree5_二叉排序树.BinarySortTree;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author wisdomelon
 * @date 2020/7/28 0028
 * @project data_study
 * @jdk 1.8
 */
public class SortMain {

    public static void main(String[] args) {

        int[] arr = BaseSort.random();

        List<BaseSort> sorts = new ArrayList<>();
        sorts.add(new BubbleSort());
        sorts.add(new SimpleChooseSort());
        sorts.add(new InsertionSort());
        sorts.add(new ShellSort());
        sorts.add(new QuickSort());
        sorts.add(new MergeSort());
        sorts.add(new RadixSort());
        sorts.add(new HeapSort());
        sorts.add(new BinarySortTree());

        int size = sorts.size();
        ExecutorService executorService = Executors.newFixedThreadPool(size);
        sorts.forEach((sort) -> executorService.submit(() -> sort.asc(arr)));
        executorService.shutdown();
    }
}
