package s3_链表;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author wisdomelon
 * @date 2020/6/14 0014
 * @project data_study
 * @jdk 1.8
 */
public class LinkeredListDemo {

    public static void main(String[] args) {
        HeroNode2 heroNode1 = new HeroNode2(1, "admin1", "adm1");
        HeroNode2 heroNode2 = new HeroNode2(2, "admin2", "adm2");
        HeroNode2 heroNode3 = new HeroNode2(3, "admin3", "adm3");
        HeroNode2 heroNode4 = new HeroNode2(4, "admin4", "adm4");
        ArrayList<HeroNode2> arrayList= new ArrayList<HeroNode2>();
        arrayList.add(heroNode1);
        arrayList.add(heroNode2);
        arrayList.add(heroNode3);
        arrayList.add( heroNode4);

        arrayList.add(new HeroNode2(7, "admin7", "adm7"));
        arrayList.add(new HeroNode2(6845, "admin6845", "adm6845"));
        arrayList.add(new HeroNode2(7, "admin7", "adm7"));

        LinkedBlockingQueue<HeroNode2> queue = new LinkedBlockingQueue<HeroNode2>();
        LinkedList<HeroNode2> linkedList = new LinkedList<HeroNode2>();
        Iterator<HeroNode2> iterator = arrayList.iterator();
        /*while(iterator.hasNext()) {
            HeroNode2 next = iterator.next();

            if(linkedList.size() == 0) {
                linkedList.add(next);
                continue;
            }
            boolean flag = false;
            for(int i=0; i<linkedList.size(); i++) {
                HeroNode2 next1 = linkedList.get(i);
                if(next.no <= next1.no) {
                    flag = true;
                    linkedList.add(i, next);
                    break;
                }
            }
            if(!flag) {
                linkedList.add(next);
                continue;
            }
        }*/
        while(iterator.hasNext()) {
            HeroNode2 next = iterator.next();

            int index = linkedList.indexOf(next);
            if(index > -1) {
                linkedList.add(index, next);
            } else {
                linkedList.add(next);
            }
        }

        ListIterator<HeroNode2> heroNodeListIterator = linkedList.listIterator();
        while(heroNodeListIterator.hasNext()) {
            HeroNode2 next = heroNodeListIterator.next();
            System.out.println(next);
        }
    }
}

class HeroNode2 {
    public int no;

    public String name;

    public String nickname;

    public HeroNode2(int hNo, String hName, String hNickname) {
        this.no = hNo;
        this.name = hName;
        this.nickname = hNickname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeroNode2 heroNode2 = (HeroNode2) o;
        return no == heroNode2.no &&
                Objects.equals(name, heroNode2.name) &&
                Objects.equals(nickname, heroNode2.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(no, name, nickname);
    }

    @Override
    public String toString() {
        return "HeroNode2{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
