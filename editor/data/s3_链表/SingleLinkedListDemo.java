package s3_链表;

import java.util.Stack;

/**
 * @author wisdomelon
 * @date 2020/6/14 0014
 * @project data_study
 * @jdk 1.8
 */
public class SingleLinkedListDemo {

    public static void main(String[] args) {

        HeroNode heroNode1 = new HeroNode(1, "admin1", "adm1");
        HeroNode heroNode2 = new HeroNode(2, "admin2", "adm2");
        HeroNode heroNode3 = new HeroNode(3, "admin3", "adm3");
        HeroNode heroNode4 = new HeroNode(4, "admin4", "adm4");

        SingleLinkedList singleLinkedList = new SingleLinkedList();
        singleLinkedList.add(heroNode1);
        singleLinkedList.add(heroNode2);
        singleLinkedList.add(heroNode3);
        singleLinkedList.add(heroNode4);

        singleLinkedList.addByOrder(new HeroNode(6845, "admin6845", "adm6845"));
        singleLinkedList.addByOrder(new HeroNode(7, "admin7", "adm7"));
        singleLinkedList.addByOrder(new HeroNode(7, "admin7", "adm7"));

        singleLinkedList.show();


        System.out.println("-----------------");

        singleLinkedList.update(new HeroNode(8, "admin8", "adm8"));
        singleLinkedList.update(new HeroNode(7, "admin9", "adm9"));
        singleLinkedList.show();


        System.out.println("-----------------");

        singleLinkedList.delete(3);
        singleLinkedList.delete(4);
        singleLinkedList.show();

        System.out.println("-----------------");
        System.out.println("-----------------");
        singleLinkedList.revertPrint();

        System.out.println("+++++++++++++++++");
        System.out.println("-----------------");
        HeroNode revert = singleLinkedList.revert();
        revert = revert.next;
        while(true) {
            if(revert == null) {
                break;
            }
            System.out.println(revert);
            revert = revert.next;
        }

        System.out.println("-----------------");
        System.out.println("-----------------");
        singleLinkedList.show();

    }
}

/** 定义链表来管理HeroNode */
class SingleLinkedList {

    /** 初始化头节点，头节点不能发生变化,  不存放具体的数据 */
    private static final HeroNode head = new HeroNode(0, "", "");

    /** 添加节点
     * 1. 不考虑顺序的情况，找到当前链表的最后节点，将最后这个节点的next指向新的节点
     * */
    public void add(HeroNode heroNode) {

        HeroNode temp = head;
        while(true) {
            if(temp.next == null) {
                break;
            }
            temp = temp.next;
        }
        // 当退出while循环时，temp就指向了链表的最后
        temp.next = heroNode;

    }

    /** 添加节点
     * 1. 考虑顺序的情况，找到当前链表的匹配编号的节点，将此节点的next指向临时节点，将临时节点的next指向之前的那个节点
     * 2. 如果有这个排名，则添加失败并给出提示
     * */
    public void addByOrder(HeroNode heroNode) {

        HeroNode temp = head;
        // 标志添加的编号是否存在，默认不存在
        boolean flag = false;
        while(true) {
            if(temp.next == null) {
                break;
            }
            if(temp.next.no > heroNode.no) {
                break;
            } else if(temp.next.no == heroNode.no) {
                flag = true;
                break;
            }
            temp = temp.next;
        }

        if(flag) {
            System.out.println("编号已存在，不能添加!");
           return;
        }

        HeroNode nextHeadNode = temp.next;
        heroNode.next = nextHeadNode;
        temp.next = heroNode;

    }

    public void update(HeroNode heroNode) {
        HeroNode temp = head;
        boolean flag = false;
        while(true) {
            if(temp.next == null) {
                break;
            }
            if(temp.next.no == heroNode.no) {
                flag = true;
                break;
            }
            temp = temp.next;
        }

        if(flag) {
            temp.next.nickname = heroNode.nickname;
            temp.next.name = heroNode.name;
        }
    }

    public void delete(int heroNodeno) {
        if(head.next == null) {
            return;
        }
        HeroNode temp = head;
        boolean flag = false;
        while(true) {
            if(temp.next == null) {
                break;
            }
            if(temp.next.no == heroNodeno    ) {
                flag = true;
                break;
            }
            temp = temp.next;
        }
        if(flag) {
            temp.next = temp.next.next;
        }
    }

    /** 显示链表 */
    public void show() {
        if(head.next == null) {
            System.out.println("链表为空");
            return;
        }
        HeroNode temp = head.next;
        while(true) {
            if(temp == null) {
                break;
            }
            System.out.println(temp);
            temp = temp.next;
        }
    }

    public HeroNode revert() {
        HeroNode revertNode = new HeroNode(0, "", "");
        if(head.next == null || head.next.next == null) {
            return revertNode;
        }

        HeroNode cur = head.next;
        HeroNode next = null;
        while(cur != null) {
            next = cur.next;
            cur.next = revertNode.next;
            revertNode.next = cur;
            cur = next;

        }
        return revertNode;

    }

    public void revertPrint() {
        if(head.next == null || head.next.next == null) {
            show();
            return;
        }

        Stack<HeroNode> stack = new Stack<>();
        HeroNode temp = head.next;
        do {
            stack.push(temp);
            temp = temp.next;
        } while(temp != null);

        while(stack.size() > 0) {
            System.out.println(stack.pop());
        }
    }


}


/** 定义一个HeroNode， 每个HeroNode 对象就是一个节点*/
class HeroNode {
    public int no;

    public String name;

    public String nickname;

    /** 指向下一个节点 */
    public HeroNode next;

    public HeroNode(int hNo, String hName, String hNickname) {
        this.no = hNo;
        this.name = hName;
        this.nickname = hNickname;
    }

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
