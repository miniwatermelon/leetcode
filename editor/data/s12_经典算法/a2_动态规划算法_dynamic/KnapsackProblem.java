package s12_经典算法.a2_动态规划算法_dynamic;

import java.util.Arrays;

/**
 * 动态规划算法解决01背包问题（同类商品只能放一个）
 * 问题描述：
 * 有一个背包，容量为4磅 ， 现有如下物品
 * 物品         重量         价格
 * 吉他（G）     1           1500
 * 音响（S）     4           3000
 * 电脑（L）     3           2000
 * （1)要求达到的目标为装入的背包的总价值最大，并且重量不超出
 * (2)要求装入的物品不能重复
 *
 *  思路分析:
 *  算法的主要思想，利用动态规划来解决。每次遍历到的第i个物品，根据w[i]和v[i]来确定是否需要将该物品放入背包中。
 *  即对于给定的n个物品，设v[i]、w[i]分别为第i个物品的价值和重量，C为背包的容量。再令v[i][j]表示在前i个物品中能够装入容量为j的背包中的最大价值。
 *  则我们有下面的结果：
 *  (1)  v[i][0]=v[0][j]=0; //表示 填入表 第一行和第一列是0
 *  (2) 当w[i]> j 时：v[i][j]=v[i-1][j]   // 当准备加入新增的商品的容量大于 当前背包的容量时，就直接使用上一个单元格的装入策略
 *  (3) 当j>=w[i]时： v[i][j]=max{v[i-1][j], v[i]+v[i-1][j-w[i]]}  // 当 准备加入的新增的商品的容量小于等于当前背包的容量 
 *  // 装入的方式:
 *  v[i-1][j]： 就是上一个单元格的装入的最大值
 *  v[i] : 表示当前商品的价值
 *  v[i-1][j-w[i]] ： 装入i-1商品，到剩余空间j-w[i]的最大值
 *
 * @author wisdomelon
 * @date 2022/2/10
 */
public class KnapsackProblem {

    public static void main(String[] args) {
        // 物品的重量
        int[] w = {1,4,3};
        // 物品的价值
        int[] val = {1500, 3000, 2000};
        // 背包的容量  // 物品的个数
        int m = 4, n = val.length;

        // 记录放入商品的情况
        int[][] path = new int[n+1][m+1];

        // 二维数组表示动态规划的盘
        // v[i][j] : 表示在前i个物品中能够装入容量为j的背包中的最大价值
        int[][] v = new int[n+1][m+1];

        // 初始化第一行和第一列
        for(int i=0; i<v.length; i++) {
            // 第一列设置为0
            v[i][0] = 0;
        }
        for(int i=0; i<v[0].length; i++) {
            // 第一行设置为0
            v[0][i] = 0;
        }

        // 动态规划处理
        for(int i=1; i<v.length; i++) { // 不处理第一行
            for(int j=1; j<v[0].length; j++) { // 不处理第一列
                if(w[i-1] > j) {
                    v[i][j] = v[i-1][j];
                } else {
                    v[i][j] = Math.max(v[i-1][j], val[i-1] + v[i-1][j-w[i-1]]);
                    // 为了记录商品存放到背包的情况，
                    if(v[i-1][j] < val[i-1] + v[i-1][j-w[i-1]]) {
                        v[i][j] = val[i-1] + v[i-1][j-w[i-1]];
                        // 把当前的情况记录到path
                        path[i][j] = 1;
                    } else {
                        v[i][j] = v[i-1][j];
                    }
                }
            }
        }

        sout(v);
        System.out.println("------------------");

        int i = path.length - 1;
        int j = path[0].length - 1;
        while(i > 0 && j > 0) {
            if(path[i][j] == 1) {
                System.out.printf("第%d个商品放入背包\n", i);
                j -= w[i-1];
            }
            i--;
        }

    }

    private static void sout(int[][] v) {
        for (int[] ints : v) {
            System.out.println(Arrays.toString(ints));
        }
    }
}
