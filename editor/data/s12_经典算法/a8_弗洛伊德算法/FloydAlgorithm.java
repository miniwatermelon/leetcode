package s12_经典算法.a8_弗洛伊德算法;

import java.util.Arrays;

/**
 * Created by 西瓜瓜
 * Date :2022/2/27
 * Description :
 * Version :1.0
 * <p>
 * 弗洛伊德算法
 */
public class FloydAlgorithm {

    public static void main(String[] args) {
        char[] vertex = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        final int N = 65535;
        int[][] matrix = {
                {0, 5, 7, N, N, N, 2},
                {5, 0, N, 9, N, N, 3},
                {7, N, 0, N, 8, N, N},
                {N, 9, N, 0, N, 4, N},
                {N, N, 8, N, 0, 5, 4},
                {N, N, N, 4, 5, 0, 6},
                {2, 3, N, N, 4, 6, 0},
        };

        // 创建图对象
        Graph graph = new Graph(vertex.length, matrix, vertex);
        //
        graph.floyd();
        graph.show();
    }


}

/**
 * 创建图
 */
class Graph {
    /**
     * 存放顶点的数组
     */
    char[] vertex;
    /**
     * 保存各个顶点出发到其他顶点的距离，最后的结果也是保留在该数组
     */
    int[][] dis;
    /**
     * 保存到达目标顶点的前驱结点
     */
    int[][] pre;

    /**
     * 构造器
     *
     * @param length 大小
     * @param matrix 邻接矩阵
     * @param vertex 顶点数组
     */
    public Graph(int length, int[][] matrix, char[] vertex) {
        this.vertex = vertex;
        this.dis = matrix;
        this.pre = new int[length][length];

        // 对pre数组初始化,存放前驱结点的下标
        for (int i = 0; i < length; i++) {
            Arrays.fill(pre[i], i);
        }
    }

    /**
     * 显示pre数组和dis数组
     */
    public void show() {
        for (int i = 0; i < dis.length; i++) {
            for (int j = 0; j < dis.length; j++) {
                System.out.printf("(%s到%s的最短路径是%s) ", vertex[i], vertex[j], dis[i][j]);
            }
            System.out.println();
        }

        System.out.println();

        for (int[] ints : pre) {
            for (int j = 0; j < pre.length; j++) {
                System.out.print(vertex[ints[j]] + " ");
            }
            System.out.println();
        }
    }

    /**
     * 弗洛伊德算法
     */
    public void floyd() {
        // 保存距离
        int len = 0;

        // 对中间顶点的遍历
        for (int k = 0; k < dis.length; k++) {
            // 从i顶点出发
            for (int i = 0; i < dis.length; i++) {
                // 到达j顶点
                for (int j = 0; j < dis.length; j++) {
                    // 求出从i顶点出发，经过k顶点，到达j顶点的距离
                    len = dis[i][k] + dis[k][j];
                    if(len < dis[i][j]) {
                        // 更新距离
                        dis[i][j] = len;
                        // 更新前驱结点
                        pre[i][j] = pre[k][j];
                    }
                }
            }
        }
    }

}
