package s12_经典算法.a1_分治算法_dac;

/**
 * <code>{@link Hanoitower}</code>
 * <p>
 * description: Hanoitower
 * <p>
 *
 * @author 西瓜瓜 on 2022/2/15 22:39
 * <p>
 * 汉罗塔的移动方法
 */
public class Hanoitower {

    public static void main(String[] args) {
        hanoitower(3, 'A', 'B', 'C');
    }

    public static void hanoitower(int num, char a, char b, char c) {
        // 如果只有一个盘
        if (num == 1) {
            System.out.println("第1个盘从 " + a + "->" + c);
        } else {
            // 如果有num>2 的情况，我们总是可看做是两个盘 1.最下面的盘 2.上面的盘
            // 1. 先把最上面的所有盘 A->B
            hanoitower(num - 1, a, c, b);
            // 2. 把最下面的盘 A->C
            System.out.println("第" + num + "个盘从 " + a + "->" + c);
            // 3. 把B塔的所有盘 从 B->C
            hanoitower(num - 1, b, a, c);
        }
    }
}
