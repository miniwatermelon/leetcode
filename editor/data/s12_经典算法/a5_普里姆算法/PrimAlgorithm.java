package s12_经典算法.a5_普里姆算法;

import java.util.Arrays;

/**
 * Created by 西瓜瓜
 * Date :2022/2/27
 * Description :
 * Version :1.0
 * <p>
 * 普里姆算法
 */
public class PrimAlgorithm {

    public static void main(String[] args) {
        char[] data = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        int verxs = data.length;
        int[][] weight = new int[][]{
                {10000,5,7,10000,10000,10000,2},
                {5,10000,10000,9,10000,10000,3},
                {7,10000,10000,10000,8,10000,10000},
                {10000,9,10000,10000,10000,4,10000},
                {10000,10000,8,10000,10000,5,4},
                {10000,10000,10000,4,5,10000,6},
                {2,3,10000,10000,4,6,10000},
        };

        // 创建对象
        MGraph mGraph = new MGraph(verxs);
        MinTree minTree = new MinTree();
        minTree.createGraph(mGraph, verxs, data, weight);
        // 输出图形
        minTree.showGraph(mGraph);

        // 普里姆算法
        minTree.prim(mGraph, 0);
    }
}

/**
 * 创建最小生成树
 */
class MinTree {

    /**
     * 创建图的邻接矩阵
     *
     * @param mGraph 图对象
     * @param verxs  图对应的顶点个数
     * @param data   图的各个顶点的值
     * @param weight 图的邻接矩阵
     */
    public void createGraph(MGraph mGraph, int verxs, char[] data, int[][] weight) {
        for (int i = 0; i < verxs; i++) {
            mGraph.data[i] = data[i];
            for (int j = 0; j < verxs; j++) {
                mGraph.weight[i][j] = weight[i][j];
            }
        }

    }

    /**
     * 显示图的邻接矩阵
     * @param mGraph
     */
    public void showGraph(MGraph mGraph) {
        for (int[] rows : mGraph.weight) {
            System.out.println(Arrays.toString(rows));
        }
    }

    /**
     * prim算法，得到最小生成树
     * @param graph 图
     * @param v 图的第几个顶点开始生成
     */
    public void prim(MGraph graph, int v) {
        // 标记顶点是否被访问过
        int[] visited  = new int[graph.verxs];
        // 当前节点标记为已访问
        visited[v] = 1;

        // h1和h2记录两个顶点的下标
        int h1 = -1;
        int h2 = -1;
        int minWeight = 10000;

        // n个顶点有n-1条边，第一层循环确定输出的条数为n-1
        for(int k=1; k<graph.verxs; k++) {

            // 确定每一次生成的子图，和哪个节点的距离最近
            for(int i=0; i<graph.verxs; i++) { // i节点表示被访问过的节点
                for(int j=0; j<graph.verxs; j++) { // j节点表示还没有访问过的节点
                    if(visited[i] == 1 && visited[j] == 0 && graph.weight[i][j] < minWeight) {
                        // 替换
                        minWeight = graph.weight[i][j];
                        h1 = i;
                        h2 = j;
                    }
                }
            }

            // 找到一条边是最小，将当前找到的这个节点标记为已访问
            System.out.println("边 <" + graph.data[h1] + "," + graph.data[h2] + "> 权值：" + graph.weight[h1][h2]);
            visited[h2] = 1;
            // 重置
            minWeight = 10000;

        }


    }
}

/**
 * 图的邻接矩阵
 */
class MGraph {
    // 表示图的节点个数
    int verxs;
    // 存放节点数据
    char[] data;
    // 存放边，就是邻接矩阵
    int[][] weight;

    public MGraph(int verxs) {
        this.verxs = verxs;
        data = new char[verxs];
        weight = new int[verxs][verxs];
    }
}
