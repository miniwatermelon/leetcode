package s12_经典算法.a7_迪杰斯特拉算法;

import java.util.Arrays;

/**
 * Created by 西瓜瓜
 * Date :2022/2/27
 * Description :
 * Version :1.0
 * <p>
 * 迪杰斯特拉算法
 */
public class DijkstraAlgorithm {
    public static final int N = 65535;

    public static void main(String[] args) {
        char[] vertex = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};

        int[][] matrix = {
                {N, 5, 7, N, N, N, 2},
                {5, N, N, 9, N, N, 3},
                {7, N, N, N, 8, N, N},
                {N, 9, N, N, N, 4, N},
                {N, N, 8, N, N, 5, 4},
                {N, N, N, 4, 5, N, 6},
                {2, 3, N, N, 4, 6, N},
        };

        // 创建图对象
        Graph graph = new Graph(vertex, matrix);
        graph.show();
        System.out.println();
        graph.dijkstra(6);
    }
}

/**
 * 已访问顶点集合
 */
class VisitedVertex {
    public static final int N = 65535;

    /** 记录各个顶点是否访问过 1表示访问过，0未访问，会动态更新 */
    int[] alreadyArr;

    /** 每个下标对应的值为前一个顶点的下标，会动态更新 */
    int[] preVisited;

    /** 记录出发顶点到其他所有顶点的距离，比如G为出发顶点，就会记录G到其他顶点的距离，会动态更新，求得最短距离就会放入dis */
    int[] dis;

    /**
     * 构造器
     * @param length 顶点的个数
     * @param index 出发顶点对应的下标
     */
    public VisitedVertex(int length, int index) {
        this.alreadyArr = new int[length];
        // 设置出发顶点被访问过
        this.alreadyArr[index] = 1;
        this.preVisited = new int[length];
        this.dis = new int[length];
        // 初始化dis数组
        Arrays.fill(this.dis, N);
        // 设置出发顶点的访问距离为0
        this.dis[index] = 0;
    }

    /**
     * 判断index顶点是否被访问过
     * @param index
     * @return 访问过则访问true
     */
    public boolean in(int index) {
        return alreadyArr[index] == 1;
    }

    /**
     * 更新出发顶点到index顶点的距离
     * @param index
     * @param len
     */
    public void updateDis(int index, int len) {
        dis[index] = len;
    }

    /**
     * 更新顶点的前驱为index节点
     * @param pre
     * @param index
     */
    public void updatePre(int pre, int index) {
        preVisited[pre] = index;
    }

    /**
     * 返回出发顶点到index顶点的距离
     * @param index
     * @return
     */
    public int getDis(int index) {
        return dis[index];
    }

    /**
     * 选择并继续返回新的访问顶点
     * @return
     */
    public int updateArr() {
        int min = N, index = 0;
        for (int i = 0; i < alreadyArr.length; i++) {
            if(alreadyArr[i] == 0 && dis[i] < min) {
                min = dis[i];
                index = i;
            }
        }

        // 更新index顶点被访问过
        alreadyArr[index] = 1;
        return index;
    }

    public void show() {
        for (int j : alreadyArr) {
            System.out.print(j + " ");
        }
        System.out.println();

        for (int pre : preVisited) {
            System.out.print(pre + " ");
        }
        System.out.println();

        for (int di : dis) {
            System.out.print(di + " ");
        }
    }
}

/**
 * 创建图
 */
class Graph {

    /**
     * 存放顶点数组
     */
    char[] vertex;
    /**
     * 邻接矩阵
     */
    int[][] matrix;

    ;

    public Graph(char[] vertex, int[][] matrix) {
        this.vertex = vertex;
        this.matrix = matrix;
    }

    public void show() {
        for (int[] link : matrix) {
            System.out.println(Arrays.toString(link));
        }
    }

    public void showDijkstra(VisitedVertex vv) {
        vv.show();
    }

    /**
     * 迪杰斯特拉算法
     * @param index 表示出发顶点对应的下标
     */
    public void dijkstra(int index) {
        VisitedVertex vv = new VisitedVertex(vertex.length, index);
        // 更新index顶点到周围顶点的距离和前驱顶点
        update(index, vv);

        for (int j = 1; j < vertex.length; j++) {
            // 选择并返回新的访问节点
            index = vv.updateArr();
            // 更新index顶点到周围顶点的距离和前驱顶点
            update(index, vv);
        }

        showDijkstra(vv);
    }

    /**
     * 更新index下标顶点到周围顶点的距离和
     * @param index
     * @param vv
     */
    private void update(int index, VisitedVertex vv) {
        int len = 0;
        for (int j = 0; j < matrix[index].length; j++) {
            // 出发顶点到index顶点的距离+从index顶点到j顶点的距离的和
            len = vv.getDis(index) + matrix[index][j];
            // 如果j顶点没有 被访问过，并且len小于出发顶点到j顶点的距离，就需要更新
            if(!vv.in(j) && len < vv.getDis(j)) {
                vv.updatePre(j, index);
                vv.updateDis(j, len);
            }
        }
    }
}
