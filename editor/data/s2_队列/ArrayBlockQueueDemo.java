package s2_队列;

import java.util.Random;
import java.util.concurrent.*;

/**
 * @author wisdomelon
 * @date 2020/6/10 0010
 * @project data_study
 * @jdk 1.8
 */
public class ArrayBlockQueueDemo {

    public static void main(String[] args) {
        final ArrayBlockingQueue<Integer> arrayBlockingQueue = new ArrayBlockingQueue<Integer>(10);
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        executorService.execute(new Runnable() {
            Random random = new Random();
            @Override
            public void run() {
                while(true) {
                    try {
                        long l = System.currentTimeMillis();
                        int anInt = random.nextInt(100);
                        arrayBlockingQueue.put(random.nextInt(100));
                        System.out.println("生产者-----------------------" + anInt + ", use time: " + (System.currentTimeMillis() - l) + "ms");
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        });
        executorService.execute(new Runnable() {
            Random random = new Random();
            @Override
            public void run() {
                while(true) {
                    try {
                        long l = System.currentTimeMillis();
                        int anInt = random.nextInt(100);
                        arrayBlockingQueue.put(random.nextInt(100));
                        System.out.println("生产者-----------------------" + anInt + ", use time: " + (System.currentTimeMillis() - l) + "ms");
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        });
        executorService.execute(new Runnable() {
            Random random = new Random();
            @Override
            public void run() {
                while(true) {
                    try {
                        long l = System.currentTimeMillis();
                        int anInt = random.nextInt(100);
                        arrayBlockingQueue.put(random.nextInt(100));
                        System.out.println("生产者-----------------------" + anInt + ", use time: " + (System.currentTimeMillis() - l) + "ms");
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        });
        executorService.execute(new Runnable() {
            Random random = new Random();
            @Override
            public void run() {
                while(true) {
                    try {
                        long l = System.currentTimeMillis();
                        int anInt = random.nextInt(100);
                        arrayBlockingQueue.put(random.nextInt(100));
                        System.out.println("生产者-----------------------" + anInt + ", use time: " + (System.currentTimeMillis() - l) + "ms");
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        });

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        long l = System.currentTimeMillis();
                        Integer take = arrayBlockingQueue.take();
                        System.out.println("消费者-----------------------" + take + ", use time: " + (System.currentTimeMillis() - l) + "ms");
                        Thread.sleep(700);
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        });
    }
}
