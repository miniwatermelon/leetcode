package s2_队列;

import java.util.Scanner;

/**
 * @author wisdomelon
 * @date 2020/6/10 0010
 * @project data_study
 * @jdk 1.8
 */
public class CircleArrayQueueDemo {


    public static void main(String[] args) {

        CircleArrayQueue arrayQueue = new CircleArrayQueue(3);
        char key = ' ';
        Scanner scanner = new Scanner(System.in);
        boolean loop = true;
        //输出菜单
        while(loop) {
            System.out.println("s(show)：显示队列");
            System.out.println("a(add)：添加队列");
            System.out.println("g(get)：取出队列");
            System.out.println("h(head)：头部队列");
            System.out.println("e(exit)：退出");
            key = scanner.next().charAt(0);
            switch (key) {
                case 's':
                    arrayQueue.showQueue();
                    break;
                case 'a':
                    try {
                        System.out.println("please input you data: ");
                        int queue = scanner.nextInt();
                        arrayQueue.addQueue(queue);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'g':
                    try {
                        int get = arrayQueue.getQueue();
                        System.out.println("取出的数据是：" + get);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'h':
                    try {
                        int get = arrayQueue.headQueue();
                        System.out.println("取出的数据是：" + get);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'e':
                    loop = false;
                    break;
                default:
                    break;
            }
        }
        scanner.close();
        System.out.println("exit~~~");
    }

}

class CircleArrayQueue{
    /** 队列最大容量 */
    private int maxSize;

    /** 指向队列头部的位置 */
    private int front;

    /** 指向队列尾部元素的后一个位置 */
    private int rear;

    /** 数组模拟队列 */
    private int[] arr;

    /** 创建队列的构造器*/
    public CircleArrayQueue(int maxSize) {
        this.maxSize = maxSize;
        this.arr = new int[maxSize];
        // 指向队列头部的位置
        this.front = 0;
        // 指向队列尾部元素的后一个位置
        this.rear = 0;
    }

    /** 判断队列是否满 */
    public boolean isFull() {
        return (this.rear + 1) % this.maxSize == this.front;
    }

    /** 判断队列是否为空 */
    public boolean isEmpty() {
        return this.rear == this.front;
    }

    /** 添加数据到队列 */
    public void addQueue(int n) {
        // 1.判断队列是否满
        if(isFull()) {
            throw new RuntimeException("队列满");
        }
        this.arr[rear] = n;
        // rear后移
        this.rear = (this.rear + 1) % this.maxSize;
    }

    /** 取出数据到队列 */
    public int getQueue() {
        // 1.判断队列是否为空
        if(isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("无数据");
        }

        int queue = this.arr[front];
        // front后移
        this.front = (this.front + 1) % this.maxSize;

        return queue;
    }

    /** 显示队列的数据 */
    public void showQueue() {
        if(isEmpty()) {
            System.out.println("无数据");
            return;
        }

        // 1. 从front开始遍历，遍历多少个元素
        for (int i = front; i < front + size(); i++) {
            System.out.println("arr[" + (i % this.maxSize) + "] = " + this.arr[i % this.maxSize]);
        }
    }

    /** 求出当前队列有效数据的个数 */
    public int size() {
        return (this.rear + this.maxSize - this.front) % this.maxSize;
    }

    /** 显示队列的头部数据，不是取出数据 */
    public int headQueue() {
        if(isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("无数据");
        }

        return this.arr[this.front];
    }
}
