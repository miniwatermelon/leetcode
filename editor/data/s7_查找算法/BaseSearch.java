package s7_查找算法;

/**
 * <code>{@link BaseSearch}</code>
 * <p>
 * description: BaseSearch
 * <p>
 *
 * @author 西瓜瓜 on 2022/2/15 22:14
 */
public abstract class BaseSearch {

    /**
     * 查找目标数，返回对应下标
     * @param arr
     * @param target
     * @return
     */
    public abstract int search(int[] arr, int target);
}
