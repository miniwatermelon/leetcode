package s5_递归;

/**
 * @author wisdomelon
 * @date 2020/7/28 0028
 * @project data_study
 * @jdk 1.8
 * 递归
 */
public class RecursionTest {

    public static void main(String[] args) {

        test(10);
    }

    public static void test(int n) {
        if(n > 2) {
            test(n-1);
        }
        System.out.println("n:" + n);
    }
}
