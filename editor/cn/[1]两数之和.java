//给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target 的那 两个 整数，并返回它们的数组下标。 
//
// 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。 
//
// 你可以按任意顺序返回答案。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [2,7,11,15], target = 9
//输出：[0,1]
//解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
// 
//
// 示例 2： 
//
// 
//输入：nums = [3,2,4], target = 6
//输出：[1,2]
// 
//
// 示例 3： 
//
// 
//输入：nums = [3,3], target = 6
//输出：[0,1]
// 
//
// 
//
// 提示： 
//
// 
// 2 <= nums.length <= 104 
// -109 <= nums[i] <= 109 
// -109 <= target <= 109 
// 只会存在一个有效答案 
// 
//
// 进阶：你可以想出一个时间复杂度小于 O(n2) 的算法吗？ 
// Related Topics 数组 哈希表 
// 👍 13368 👎 0


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution1 {

    /**
     * 解题思路：双层for循环
     * 第一层for循环计算target与当前元素的差值
     * 第二层for循环比较差值，一致则返回
     * 注意：第一层for循环的结束条件是 nums.length - 1, 第二层for循环的开始条件是 int j = i + 1, 始终保证是两个不同的元素在进行计算。
     * <p>
     * 执行耗时:37 ms,击败了44.35% 的Java用户
     * 内存消耗:41.5 MB,击败了53.18% 的Java用户
     */
    public int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length - 1; i++) {
            int diff = target - nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                if (diff == nums[j]) {
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }


    /**
     * 解题思路：
     * 1. 双指针遍历数组，计算每一个 target-nums[end]的值
     * 2. 对比nums[begin]的值，直到相同则返回
     * <p>
     * 解答成功:
     * 执行耗时:5 ms,击败了51.74% 的Java用户
     * 内存消耗:41.5 MB,击败了5.51% 的Java用户
     *
     */
    public int[] twoSum2(int[] nums, int target) {
        int begin = 0, end = nums.length - 1;
        while (end > begin) {
            int diff = target - nums[end];
            while (end > begin) {
                if (nums[begin] == diff) {
                    return new int[]{begin, end};
                }
                begin++;
                if (end == begin) {
                    begin = 0;
                    end--;
                    break;
                }
            }
        }
        return null;
    }

    /**
     * 解题思路：
     * 1. hashmap存储 key=数组元素,value=数组下标
     * 2. 判断 target - nums[i] 的值是否在hashmap中存在，存在则返回存储的下标和当前下标i
     * 3. 不存在则将nums[i] 和下标i存入hashmap中，以供下个元素去判断
     * <p>
     * 执行耗时:1 ms,击败了99.34% 的Java用户
     * 内存消耗:41.6 MB,击败了5.02% 的Java用户
     *
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSumAnsuer(int[] nums, int target) {

        Map<Integer, Integer> numsMap = new HashMap<>(nums.length);
        for (int i = 0; i < nums.length; i++) {
            if (numsMap.containsKey(target - nums[i])) {
                return new int[]{numsMap.get(target - nums[i]), i};
            }
            numsMap.put(nums[i], i);
        }
        return null;
    }

    public static void main(String[] args) {
//        System.out.println(Arrays.toString(new Solution1().twoSum(new int[]{-3, 4, 3, 90}, 0)));
//        System.out.println(Arrays.toString(new Solution1().twoSum(new int[]{2, 7, 11, 15}, 9)));
//        System.out.println(Arrays.toString(new Solution1().twoSum(new int[]{3, 2, 4}, 6)));
//        System.out.println(Arrays.toString(new Solution1().twoSum(new int[]{3, 3}, 6)));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
