//给定一个字符串 s ，你需要反转字符串中每个单词的字符顺序，同时仍保留空格和单词的初始顺序。 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "Let's take LeetCode contest"
//输出："s'teL ekat edoCteeL tsetnoc"
// 
//
// 示例 2: 
//
// 
//输入： s = "God Ding"
//输出："doG gniD"
// 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 5 * 104 
// s 包含可打印的 ASCII 字符。 
// s 不包含任何开头或结尾空格。 
// s 里 至少 有一个词。 
// s 中的所有单词都用一个空格隔开。 
// 
// Related Topics 双指针 字符串 
// 👍 402 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution557 {
    /**
     * 解题思路：
     * 1.找到每个空格字符 或 是否到达末尾
     * 2.双指针交换
     *
     * 解答成功:
     * 			执行耗时:2 ms,击败了100.00% 的Java用户
     * 			内存消耗:42.1 MB,击败了6.99% 的Java用户
     * @param s
     * @return
     */
    public String reverseWords(String s) {
        if(s.length() <= 1) return s;

        char[] chars = s.toCharArray();
        int spaceIndex = 0;
        char temp;
        for (int i = 0; i <= chars.length; i++) {
            if(i == chars.length || chars[i] == ' ') {
                int l = spaceIndex, r = i-1;
                while(l < r) {
                    temp = chars[l];
                    chars[l] = chars[r];
                    chars[r] = temp;
                    l++;
                    r--;
                }

                spaceIndex = i+1;
            }
        }

        return String.valueOf(chars);
    }
}
//leetcode submit region end(Prohibit modification and deletion)
