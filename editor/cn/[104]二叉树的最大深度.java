//给定一个二叉树，找出其最大深度。 
//
// 二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。 
//
// 说明: 叶子节点是指没有子节点的节点。 
//
// 示例： 
//给定二叉树 [3,9,20,null,null,15,7]， 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7 
//
// 返回它的最大深度 3 。 
// Related Topics 树 深度优先搜索 广度优先搜索 二叉树 
// 👍 1120 👎 0


//leetcode submit region begin(Prohibit modification and deletion)

import java.util.HashMap;
import java.util.Map;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution104 {
    /**
     * 解题思路
     * 1.利用树的前序遍历，记录所有子节点的深度
     * 2.获取所有深度的最大值
     *
     * 解答成功:
     * 			执行耗时:2 ms,击败了21.14% 的Java用户
     * 			内存消耗:40.7 MB,击败了29.68% 的Java用户
     * @param root
     * @return
     */
    public int maxDepth(TreeNode root) {
        Map<TreeNode, Integer> map = new HashMap<>();
        if(root == null) return 0;

        preOrder(root, map, 1);
        Integer max = map.values().stream().max(Integer::compareTo).get();
        return max;
    }

    private void preOrder(TreeNode node, Map<TreeNode, Integer> map, Integer deep) {
        if(node.left == null && node.right == null) {
            // 添加子节点
            map.put(node, deep);
        }

        if(node.left != null) {
            preOrder(node.left, map, deep+1);
        }

        if(node.right != null) {
            preOrder(node.right, map, deep+1);
        }
    }
}
//leetcode submit region end(Prohibit modification and deletion)
