//整数数组 nums 按升序排列，数组中的值 互不相同 。 
//
// 在传递给函数之前，nums 在预先未知的某个下标 k（0 <= k < nums.length）上进行了 旋转，使数组变为 [nums[k], nums[
//k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]（下标 从 0 开始 计数）。例如， [0,1,2
//,4,5,6,7] 在下标 3 处经旋转后可能变为 [4,5,6,7,0,1,2] 。 
//
// 给你 旋转后 的数组 nums 和一个整数 target ，如果 nums 中存在这个目标值 target ，则返回它的下标，否则返回 -1 。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [4,5,6,7,0,1,2], target = 0
//输出：4
// 
//
// 示例 2： 
//
// 
//输入：nums = [4,5,6,7,0,1,2], target = 3
//输出：-1 
//
// 示例 3： 
//
// 
//输入：nums = [1], target = 0
//输出：-1
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 5000 
// -10^4 <= nums[i] <= 10^4 
// nums 中的每个值都 独一无二 
// 题目数据保证 nums 在预先未知的某个下标上进行了旋转 
// -10^4 <= target <= 10^4 
// 
//
// 
//
// 进阶：你可以设计一个时间复杂度为 O(log n) 的解决方案吗？ 
// Related Topics 数组 二分查找 
// 👍 1875 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution33 {
    /**
     * 解题思路：
     * 1.找到调转的位置
     * 2.左右两边分别进行二分查找
     *
     *
     * 解答成功:
     * 			执行耗时:0 ms,击败了100.00% 的Java用户
     * 			内存消耗:40.9 MB,击败了24.30% 的Java用户
     * @param nums
     * @param target
     * @return
     */
    public int search(int[] nums, int target) {
        if(nums.length == 1) {
            if(nums[0] == target) return 0;
            return -1;
        }

        // 找到调转的位置
        int targetIndex = nums.length - 1;
        for (int i = nums.length - 1; i >= 1; i--) {
            if(nums[i-1] > nums[i]) {
                targetIndex = i-1;
                break;
            }
        }

        // [0, targetIndex] 从小到大，  [targetIndex + 1, nums.length - 1] 从小到大
        // 二分查找
        int resIndex = getResIndex(nums, target, 0, targetIndex);

        if(targetIndex == nums.length - 1) return resIndex;
        if(resIndex != -1) return resIndex;

        resIndex = getResIndex(nums, target, targetIndex + 1, nums.length - 1);

        return resIndex;
    }

    private int getResIndex(int[] nums, int target, int l, int r) {
        while (l <= r) {
            int midIndex = (r+l) / 2;
            if(nums[midIndex] == target) {
                return midIndex;
            }
            if(nums[midIndex] > target) {
                r = midIndex - 1;
            } else {
                l = midIndex + 1;
            }
        }
        return -1;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
