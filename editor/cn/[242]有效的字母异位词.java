//给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。 
//
// 注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。 
//
// 
//
// 示例 1: 
//
// 
//输入: s = "anagram", t = "nagaram"
//输出: true
// 
//
// 示例 2: 
//
// 
//输入: s = "rat", t = "car"
//输出: false 
//
// 
//
// 提示: 
//
// 
// 1 <= s.length, t.length <= 5 * 10⁴ 
// s 和 t 仅包含小写字母 
// 
//
// 
//
// 进阶: 如果输入字符串包含 unicode 字符怎么办？你能否调整你的解法来应对这种情况？ 
//
// Related Topics 哈希表 字符串 排序 👍 942 👎 0


import java.util.HashMap;
import java.util.Map;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution242 {

    public boolean isAnagram1(String s, String t) {

        int[] hash = new int[26];
        if(s.length() != t.length()) {
            return false;
        }

        for(int i=0;i<s.length();i++) {
            if(s.charAt(i) != t.charAt(i)) {
                hash[s.charAt(i) - 'a']++;
                hash[t.charAt(i) - 'a']--;
            }
        }

        for (int j : hash) {
            if (j != 0) {
                return false;
            }
        }

        return true;
    }

    public boolean isAnagram(String s, String t) {

        Map<Character, Integer> resultMap = new HashMap<>();
        for (char schar : s.toCharArray()) {
            Integer count = resultMap.get(schar);
            if(count == null) {
                count = 0;
            }
            resultMap.put(schar, ++count);
        }

        for (char tchar : t.toCharArray()) {
            Integer count = resultMap.get(tchar);
            if(count == null) {
                return false;
            }
            count--;
            if(count == 0) {
                resultMap.remove(tchar);
            }else {
                resultMap.put(tchar, count);
            }
        }

        return resultMap.isEmpty();
    }
}
//leetcode submit region end(Prohibit modification and deletion)
