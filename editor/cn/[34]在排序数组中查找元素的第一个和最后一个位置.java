//给定一个按照升序排列的整数数组 nums，和一个目标值 target。找出给定目标值在数组中的开始位置和结束位置。 
//
// 如果数组中不存在目标值 target，返回 [-1, -1]。 
//
// 进阶： 
//
// 
// 你可以设计并实现时间复杂度为 O(log n) 的算法解决此问题吗？ 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [5,7,7,8,8,10], target = 8
//输出：[3,4] 
//
// 示例 2： 
//
// 
//输入：nums = [5,7,7,8,8,10], target = 6
//输出：[-1,-1] 
//
// 示例 3： 
//
// 
//输入：nums = [], target = 0
//输出：[-1,-1] 
//
// 
//
// 提示： 
//
// 
// 0 <= nums.length <= 105 
// -109 <= nums[i] <= 109 
// nums 是一个非递减数组 
// -109 <= target <= 109 
// 
// Related Topics 数组 二分查找 
// 👍 1468 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution34 {
    /**
     * 解题思路：
     * 利用二分查找找到一个目标值
     * 由于数组是有序排列的，则从目标值处左右发散，寻找边界后返回数组
     *
     * 解答成功:
     * 			执行耗时:0 ms,击败了100.00% 的Java用户
     * 			内存消耗:44.6 MB,击败了13.14% 的Java用户
     * @param nums
     * @param target
     * @return
     */
    public int[] searchRange(int[] nums, int target) {
        if(nums.length == 0) return new int[]{-1,-1};

        // 二分查找
        int[] targetArr = binarySearch(nums, target);

        return targetArr;
    }

    /**
     * 二分查找
     * @param nums
     * @param target
     * @return
     */
    private int[] binarySearch(int[] nums, int target) {
        int[] targetArr = new int[2];

        int l = 0, r = nums.length-1;
        while(l <= r) {
            int mid = (r+l)/2;
            if(nums[mid] == target) {
                int leftIdx = mid-1;
                int rightIdx = mid+1;
                while(leftIdx >=0 && nums[leftIdx] == target) {
                    leftIdx--;
                }
                while(rightIdx <nums.length && nums[rightIdx] == target) {
                    rightIdx++;
                }
                targetArr[0] = leftIdx+1;
                targetArr[1] = rightIdx-1;
                return targetArr;
            }
            if(nums[mid] > target) {
                r = --mid;
            } else {
                l = ++mid;
            }
        }

        return new int[]{-1, -1};
    }

    public static void main(String[] args) {
        new Solution34().searchRange(new int[]{5,7,7,8,8,10}, 6);
    }
}
//leetcode submit region end(Prohibit modification and deletion)
