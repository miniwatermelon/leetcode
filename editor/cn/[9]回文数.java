//给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。 
//
// 回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。例如，121 是回文，而 123 不是。 
//
// 
//
// 示例 1： 
//
// 
//输入：x = 121
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：x = -121
//输出：false
//解释：从左向右读, 为 -121 。 从右向左读, 为 121- 。因此它不是一个回文数。
// 
//
// 示例 3： 
//
// 
//输入：x = 10
//输出：false
//解释：从右向左读, 为 01 。因此它不是一个回文数。
// 
//
// 示例 4： 
//
// 
//输入：x = -101
//输出：false
// 
//
// 
//
// 提示： 
//
// 
// -231 <= x <= 231 - 1 
// 
//
// 
//
// 进阶：你能不将整数转为字符串来解决这个问题吗？ 
// Related Topics 数学 
// 👍 1792 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution9 {
    /**
     * 解题思路：
     * 1. 负数和末尾为0的数不是回文数
     * 2. 对源数据进行倒转后比较
     * <p>>
     * 执行耗时:5 ms,击败了82.25% 的Java用户
     * 内存消耗:40.3 MB,击败了10.11% 的Java用户
     */
    public boolean isPalindrome(int x) {
        // 用字符串的方式
        // return new StringBuilder(x+"").reverse().toString().equals(x+"");

        if(x*-1 > 0) return false;
        if(x>0 && x%10 == 0) return false;

        int res = 0;
        int source = x;
        while(source != 0) {
            res = res*10 + source%10;
            source /= 10;
        }
        return res == x;
    }

    public static void main(String[] args) {
        System.out.println(new Solution9().isPalindrome(1000021));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
