//给你一个正整数 n ，生成一个包含 1 到 n2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。 
//
// 
//
// 示例 1： 
//
// 
//输入：n = 3
//输出：[[1,2,3],[8,9,4],[7,6,5]]
// 
//
// 示例 2： 
//
// 
//输入：n = 1
//输出：[[1]]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= n <= 20 
// 
// Related Topics 数组 矩阵 模拟 
// 👍 612 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {

    public static void main(String[] args) {
//        new Solution59().generateMatrix(4);
    }

    public int[][] generateMatrix(int n) {
        int[][] res = new int[n][n];
        int num=1;
        int loop = n/2,offset=1,deep=0;
        while(offset <= loop) {
            int i=deep, j=deep;
            for(;j<n-offset;j++) {
                res[i][j] = num++;
            }

            for(;i<n-offset;i++) {
                res[i][j] = num++;
            }

            for(;j>deep;j--) {
                res[i][j] = num++;
            }

            for(;i>deep;i--) {
                res[i][j] = num++;
            }

            offset++;
            deep++;
        }

        if(n %2 == 1) {
            res[deep][deep] = num;
        }

        return res;


    }
    /**
     * 解题思路
     * 按照顺序构造二维数组数据
     *
     *
     * 解答成功:
     * 			执行耗时:0 ms,击败了100.00% 的Java用户
     * 			内存消耗:39.2 MB,击败了28.54% 的Java用户
     * @param n
     * @return
     */
    public int[][] generateMatrix1(int n) {
        int size = n*n,
                left = 0, right = n-1, top = 0, buttom = n-1;
        int num = 0;
        int[][] res = new int[n][n];

        while(size > 0) {

            // 从左往右
            for(int j = left; j <= right; j++) {
                res[top][j] = ++num;
                size--;
            }
            if(size == 0) break;
            top++;

            // 从上往下
            for(int i = top; i<= buttom; i++) {
                res[i][right] = ++num;
                size--;
            }
            if(size == 0) break;
            right--;

            // 从右往左
            for(int j = right; j >= left; j--) {
                res[buttom][j] = ++num;
                size--;
            }
            if(size == 0) break;
            buttom--;

            // 从下往上
            for(int i = buttom; i >= top; i--) {
                res[i][left] = ++num;
                size--;
            }
            left++;
        }

        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
