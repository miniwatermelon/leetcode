//给定一个候选人编号的集合 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。 
//
// candidates 中的每个数字在每个组合中只能使用 一次 。 
//
// 注意：解集不能包含重复的组合。 
//
// 
//
// 示例 1: 
//
// 
//输入: candidates = [10,1,2,7,6,1,5], target = 8,
//输出:
//[
//[1,1,6],
//[1,2,5],
//[1,7],
//[2,6]
//] 
//
// 示例 2: 
//
// 
//输入: candidates = [2,5,2,1,2], target = 5,
//输出:
//[
//[1,2,2],
//[5]
//] 
//
// 
//
// 提示: 
//
// 
// 1 <= candidates.length <= 100 
// 1 <= candidates[i] <= 50 
// 1 <= target <= 30 
// 
//
// Related Topics 数组 回溯 👍 1576 👎 0


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution40 {
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> resultList = new ArrayList<>();
        List<Integer> nowList = new ArrayList<>();
        int index = 0;

        combination1(resultList, nowList, candidates, target, index);

        return resultList;
    }

    private void combination1(List<List<Integer>> resultList, List<Integer> nowList, int[] candidates, int target, int index) {
        if (target == 0) {
            resultList.add(new ArrayList<>(nowList));
            return;
        }

        for (int i = index; i < candidates.length; i++) {
            if (target - candidates[i] < 0) {
                break;
            }
            if (i > 0 && i > index && candidates[i] == candidates[i - 1]) {
                continue;
            }
            nowList.add(candidates[i]);
            combination1(resultList, nowList, candidates, target - candidates[i], i + 1);
            nowList.remove(nowList.size() - 1);
        }
    }

    public static void main(String[] args) {
//        System.out.println(new Solution40().combinationSum2(new int[]{10, 1, 2, 7, 6, 1, 5}, 8));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
