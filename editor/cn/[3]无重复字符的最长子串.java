//给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。 
//
// 
//
// 示例 1: 
//
// 
//输入: s = "abcabcbb"
//输出: 3 
//解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
// 
//
// 示例 2: 
//
// 
//输入: s = "bbbbb"
//输出: 1
//解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
// 
//
// 示例 3: 
//
// 
//输入: s = "pwwkew"
//输出: 3
//解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
//     请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
// 
//
// 
//
// 提示： 
//
// 
// 0 <= s.length <= 5 * 10⁴ 
// s 由英文字母、数字、符号和空格组成 
// 
// Related Topics 哈希表 字符串 滑动窗口 👍 7692 👎 0


import java.util.HashMap;
import java.util.Map;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution3 {
    /**
     * 解题思路
     * 1.循环这个字符串，用一个stringbuilder记录目前未重复的字符串
     * 2.当出现重复字符时，从stringbuilder中剔除重复字符位置之前的字符串,包括该位置的字符串一并剔除
     * 3.计算stringbuilder的总长度，最大长度即为输出结果
     * <p>
     * 执行耗时:6 ms,击败了40.53% 的Java用户
     * 内存消耗:42.2 MB,击败了5.00% 的Java用户
     */
    public int lengthOfLongestSubstring2(String s) {
        if (s.length() <= 1) return s.length();
        char[] chars = s.toCharArray();
        int res = 0;
        // 定义stringbuilder，记录目前采集到的不重复字符的字符串，其中可能会出现最长字符串，但最终记录的不一定是最长字符串
        StringBuilder builder = new StringBuilder();
        for (char aChar : chars) {
            int idx = builder.indexOf(aChar + "");
            if (idx != -1) {
                // 字符串出现重复，则从头开始找，知道找到当前重复的字符串位置，将前面一串字符串全部删除
                builder.delete(0, idx + 1);
            }
            // 将当前遍历到的字符添加到字符串中
            builder.append(aChar);
            // 长度判断，取当前字符串的长度与已经记录到的最长长度比较。
            if (res < builder.length()) {
                res = builder.length();
            }
        }
        return res;
    }

    /**
     * 解题思路
     * 1.stringbuilder的精简版，记录每个字符出现的下标
     * 2.当出现重复数字时，比较重复数字的下标和当前startIndex谁大，谁大就从哪个地方开始
     * 3.map中记录的下标要是原始下标+1,否则最终计算的结果会多出1位
     * <p>
     * 执行耗时:4 ms,击败了86.19% 的Java用户
     * 内存消耗:41.6 MB,击败了33.17% 的Java用户
     */
    public int lengthOfLongestSubstring(String s) {
        char[] chars = s.toCharArray();
        int res = 0;
        int startIndex = 0;
        Map<Character, Integer> paramMap = new HashMap<>();
        for (int endIndex = 0; endIndex < chars.length; endIndex++) {
            char element = chars[endIndex];
            if(paramMap.containsKey(element)) {
                startIndex = Math.max(paramMap.get(element), startIndex);
            }
            res = Math.max(res, (endIndex - startIndex) + 1);
            paramMap.put(element, endIndex+1);
        }
        return res;
    }


//    public int lengthOfLongestSubstring(String s) {
//        int n = s.length(), ans = 0;
//        Map<Character, Integer> map = new HashMap<>();
//        for (int end = 0, start = 0; end < n; end++) {
//            char alpha = s.charAt(end);
//            if (map.containsKey(alpha)) {
//                start = Math.max(map.get(alpha), start);
//            }
//            ans = Math.max(ans, end - start + 1);
//            map.put(s.charAt(end), end + 1);
//        }
//        return ans;
//    }

    public static void main(String[] args) {
        System.out.println(new Solution3().lengthOfLongestSubstring("abcabcbb"));
        System.out.println(new Solution3().lengthOfLongestSubstring("pwwkew"));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
