//给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。 
//
// 你可以假设数组是非空的，并且给定的数组总是存在多数元素。 
//
// 
//
// 示例 1： 
//
// 
//输入：[3,2,3]
//输出：3 
//
// 示例 2： 
//
// 
//输入：[2,2,1,1,1,2,2]
//输出：2
// 
//
// 
//
// 进阶： 
//
// 
// 尝试设计时间复杂度为 O(n)、空间复杂度为 O(1) 的算法解决此问题。 
// 
// Related Topics 数组 哈希表 分治 计数 排序 
// 👍 1320 👎 0


import java.util.HashMap;
import java.util.Map;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution169 {
    /**
     * 解题思路：
     * 利用hashmap统计元素，超过半数则返回
     *
     * 解答成功:
     * 			执行耗时:12 ms,击败了22.32% 的Java用户
     * 			内存消耗:46.9 MB,击败了5.02% 的Java用户
     * @param nums
     * @return
     */
    public int majorityElement(int[] nums) {

        Map<Integer, Integer> map = new HashMap<>();
        int length = nums.length;
        for (int num : nums) {
            Integer sum = map.getOrDefault(num, 0);
            sum++;
            if (sum > length / 2) {
                return num;
            }
            map.put(num, sum);
        }

        return -1;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
