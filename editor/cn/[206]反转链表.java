//给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
// 
// 
// 
//
// 示例 1： 
//
// 
//输入：head = [1,2,3,4,5]
//输出：[5,4,3,2,1]
// 
//
// 示例 2： 
//
// 
//输入：head = [1,2]
//输出：[2,1]
// 
//
// 示例 3： 
//
// 
//输入：head = []
//输出：[]
// 
//
// 
//
// 提示： 
//
// 
// 链表中节点的数目范围是 [0, 5000] 
// -5000 <= Node.val <= 5000 
// 
//
// 
//
// 进阶：链表可以选用迭代或递归方式完成反转。你能否用两种方法解决这道题？ 
// 
// 
// Related Topics 递归 链表 
// 👍 2268 👎 0


//leetcode submit region begin(Prohibit modification and deletion)

import java.util.List;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution206 {

    public ListNode reverseList(ListNode head) {
        if(head == null || head.next == null) {
            return head;
        }

        ListNode prev = null;
        ListNode cur = head;
        while(cur != null) {
            ListNode nextNode = cur.next;
            cur.next = prev;
            prev = cur;
            cur = nextNode;
        }
        return prev;
    }

    /**
     * 解题思路:
     * 1.定义链表最后一个元素，赋值head.val
     * 2.循环head.next,每次循环添加一个前置链表元素
     * 3.head向后移动
     *
     * 解答成功:
     * 			执行耗时:0 ms,击败了100.00% 的Java用户
     * 			内存消耗:40.9 MB,击败了10.90% 的Java用户
     * @param head
     * @return
     */
    public ListNode reverseList1(ListNode head) {
        if(head == null || head.next == null) return head;

//        ListNode node = new ListNode(head.val);
//        while(head.next != null) {
//            ListNode prevNode = new ListNode(head.next.val);
//            prevNode.next = node;
//            node = prevNode;
//            head = head.next;
//        }

        ListNode prev = new ListNode(-1);
        ListNode cur = head;
        ListNode next = null;
        while(cur != null) {
            next = cur.next;
            cur.next = prev.next;
            prev.next = cur;
            cur = next;
        }

        return prev.next;
    }

    public static void main(String[] args) {
        ListNode listNode11 = new ListNode(1);
        ListNode listNode12 = new ListNode(2);
        ListNode listNode13 = new ListNode(3);
        ListNode listNode14 = new ListNode(4);
        ListNode listNode15 = new ListNode(5);
        listNode14.next = listNode15;
        listNode13.next = listNode14;
        listNode12.next = listNode13;
        listNode11.next = listNode12;

        ListNode listNode21 = new ListNode(3);
        ListNode listNode22 = new ListNode(2);
        ListNode listNode23 = new ListNode(1);
        listNode22.next = listNode23;
        listNode21.next = listNode22;

//        new Solution206().reverseList(listNode11);

    }
}
//leetcode submit region end(Prohibit modification and deletion)
