//给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。 
//
// 说明： 
//
// 你的算法应该具有线性时间复杂度。 你可以不使用额外空间来实现吗？ 
//
// 示例 1: 
//
// 输入: [2,2,1]
//输出: 1
// 
//
// 示例 2: 
//
// 输入: [4,1,2,1,2]
//输出: 4 
// Related Topics 位运算 数组 
// 👍 2263 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution136 {
    /**
     * 解题思路：
     * 对数组进行异或运算
     *
     * 解答成功:
     * 			执行耗时:1 ms,击败了100.00% 的Java用户
     * 			内存消耗:41.4 MB,击败了13.14% 的Java用户
     * @param nums
     * @return
     */
    public int singleNumber(int[] nums) {
        if(nums.length == 1) return nums[0];

        int target = nums[0];
        for (int i = 1; i < nums.length; i++) {
            target = target ^ nums[i];
        }

        return target;
    }

    public static void main(String[] args) {
        new Solution136().singleNumber(new int[]{1,2,4,1,2});
    }
}
//leetcode submit region end(Prohibit modification and deletion)
