//给你一个整数数组 nums ，你需要找出一个 连续子数组 ，如果对这个子数组进行升序排序，那么整个数组都会变为升序排序。 
//
// 请你找出符合题意的 最短 子数组，并输出它的长度。 
//
// 
//
// 
// 
// 示例 1： 
//
// 
//输入：nums = [2,6,4,8,10,9,15]
//输出：5
//解释：你只需要对 [6, 4, 8, 10, 9] 进行升序排序，那么整个表都会变为升序排序。
//[2,6,4,8,10,9,15] --> [6,4,8,10,9]      2 6 8 10 15      4 9
//[2,4,6,8,10,9,15] --> [10,9]            2 4 6 8 10 15    9
//[6,4,8,10,2,9,15] --> [6,4,8,10,2,9]    6 8 10 15        4 2 9
//[15,6,4,8,10,2,9] --> [15,6,4,8,10,2,9] 15



// 示例 2： 
//
// 
//输入：nums = [1,2,3,4]
//输出：0
// 
//
// 示例 3： 
//
// 
//输入：nums = [1]
//输出：0
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 104 
// -105 <= nums[i] <= 105 
// 
//
// 
//
// 进阶：你可以设计一个时间复杂度为 O(n) 的解决方案吗？ 
// 
// 
// Related Topics 栈 贪心 数组 双指针 排序 单调栈 
// 👍 656 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution581 {
    // [2,6,4,8,10,9,15]
    public int findUnsortedSubarray(int[] nums) {
        int len = nums.length;

        // 左边界，从后往前的遍历，min标记最小的数，left记录上一次波动的位置
        int min = nums[len-1], left = 0;
        // 右边界，从前往后的遍历，max标记最大的数，right 记录上一次波动的位置
        int max = nums[0], right = -1;

        for (int i = 0; i < len; i++) {
            // 出现波动，更新边界
            if (max > nums[i]) {
                right = i;
            }
            else {
                // 更新最大值
                max = nums[i];
            }

            // 从后往前，同理
            if (min < nums[len - i - 1]) {
                left = len - i - 1;
            }
            else {
                min = nums[len - i - 1];
            }
        }

        return right - left + 1;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
