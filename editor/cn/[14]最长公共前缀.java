//编写一个函数来查找字符串数组中的最长公共前缀。 
//
// 如果不存在公共前缀，返回空字符串 ""。 
//
// 
//
// 示例 1： 
//
// 
//输入：strs = ["flower","flow","flight"]
//输出："fl"
// 
//
// 示例 2： 
//
// 
//输入：strs = ["dog","racecar","car"]
//输出：""
//解释：输入不存在公共前缀。 
//
// 
//
// 提示： 
//
// 
// 1 <= strs.length <= 200 
// 0 <= strs[i].length <= 200 
// strs[i] 仅由小写英文字母组成 
// 
// Related Topics 字符串 
// 👍 2028 👎 0


import java.util.Map;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution14 {
    /**
     * 解题思路：
     * 1. 获取字符串数组中的最小字符串长度，作为while循环的结束条件
     * 2. 挨个获取每个字符串的第index个元素，进行比较，不一致即直接返回
     * 3. 全部一致则放入stringbuild中，index++
     * 解答成功:
     * 			执行耗时:1 ms,击败了69.10% 的Java用户
     * 			内存消耗:39.3 MB,击败了9.18% 的Java用户
     * @param strs
     * @return
     */
    public String longestCommonPrefix(String[] strs) {
        StringBuilder result = new StringBuilder("");
        int index = 0;
        int len = Integer.MAX_VALUE;
        while(index < len) {
            char c = 'A';
            for (String str : strs) {
                if(str.length() == 0) return "";
                len = Integer.min(len, str.length());

                if(c == 'A') {
                    c = str.charAt(index);
                } else {
                    if(c != str.charAt(index)) {
                        return result.toString();
                    }
                }
            }
            result.append(c);
            index++;
        }
        return result.toString();
    }
}
//leetcode submit region end(Prohibit modification and deletion)
