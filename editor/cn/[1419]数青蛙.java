//给你一个字符串 croakOfFrogs，它表示不同青蛙发出的蛙鸣声（字符串 "croak" ）的组合。由于同一时间可以有多只青蛙呱呱作响，所以 
//croakOfFrogs 中会混合多个 “croak” 。 
//
// 请你返回模拟字符串中所有蛙鸣所需不同青蛙的最少数目。 
//
// 要想发出蛙鸣 "croak"，青蛙必须 依序 输出 ‘c’, ’r’, ’o’, ’a’, ’k’ 这 5 个字母。如果没有输出全部五个字母，那么它就不会
//发出声音。如果字符串 croakOfFrogs 不是由若干有效的 "croak" 字符混合而成，请返回 -1 。 
//
// 
//
// 示例 1： 
//
// 
//输入：croakOfFrogs = "croakcroak"
//输出：1 
//解释：一只青蛙 “呱呱” 两次
// 
//
// 示例 2： 
//
// 
//输入：croakOfFrogs = "crcoakroak"
//输出：2 
//解释：最少需要两只青蛙，“呱呱” 声用黑体标注
//第一只青蛙 "crcoakroak"
//第二只青蛙 "crcoakroak"
// 
//
// 示例 3： 
//
// 
//输入：croakOfFrogs = "croakcrook"
//输出：-1
//解释：给出的字符串不是 "croak" 的有效组合。
// 
//
// 
//
// 提示： 
//
// 
// 1 <= croakOfFrogs.length <= 10⁵ 
// 字符串中的字符只有 'c', 'r', 'o', 'a' 或者 'k' 
// 
// Related Topics 字符串 计数 👍 119 👎 0


import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution1419 {

    /**
     * 解题思路
     * 字符串替换
     * <p>
     * 执行耗时:2069 ms,击败了5.18% 的Java用户
     * 内存消耗:42.7 MB,击败了51.14% 的Java用户
     */
    public int minNumberOfFrogs(String croakOfFrogs) {
        Map<Character, Character> maps = new HashMap<>(10);
        maps.put('c', 'r');
        maps.put('r', 'o');
        maps.put('o', 'a');
        maps.put('a', 'k');
        maps.put('k', 'c');


        if(croakOfFrogs.charAt(0) != 'c' || croakOfFrogs.charAt(croakOfFrogs.length() - 1) != 'k' || croakOfFrogs.length() % 5 != 0) {
            return -1;
        }

        char[] crockArr = new char[(croakOfFrogs.length() / 5)];
        for (char crock : croakOfFrogs.toCharArray()) {

            for (int i = 0; i < crockArr.length; i++) {
                // crockArr数组单元格是空
                if(crockArr[i] == '\u0000') {
                    // 要入数组的字符不是c开头
                    if(crock != 'c') {
                        return -1;
                    }
                    // 是c开头，存入下一个字符
                    crockArr[i] = maps.get(crock);
                    break;
                }

                if(crockArr[i] == crock) {
                    // 有相等的字符，存入下一个字符
                    crockArr[i] = maps.get(crock);
                    break;
                }

                // 未匹配到相等的字符，且已经遍历到最后一个节点了
                if(i == crockArr.length - 1) {
                    return -1;
                }


            }
        }

        int result = 0;
        // 取出有多少c开头就有多少青蛙
        for (char crock : crockArr) {
            if(crock == 'c') {
                result += 1;
            }
        }

        return result;
    }

    public static void main(String[] args) {
//        System.out.println(new Solution().minNumberOfFrogs("croakcroak"));
//        System.out.println(new Solution().minNumberOfFrogs("crcoakroak"));
//        System.out.println(new Solution().minNumberOfFrogs("croakcrook"));
        System.out.println(new Solution1419().minNumberOfFrogs("ccccccccck"));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
