//请你设计并实现一个满足 LRU (最近最少使用) 缓存 约束的数据结构。 
//
// 实现 LRUCache 类： 
//
// 
// 
// 
// LRUCache(int capacity) 以 正整数 作为容量 capacity 初始化 LRU 缓存 
// int get(int key) 如果关键字 key 存在于缓存中，则返回关键字的值，否则返回 -1 。 
// void put(int key, int value) 如果关键字 key 已经存在，则变更其数据值 value ；如果不存在，则向缓存中插入该组 ke
//y-value 。如果插入操作导致关键字数量超过 capacity ，则应该 逐出 最久未使用的关键字。 
// 
//
// 函数 get 和 put 必须以 O(1) 的平均时间复杂度运行。 
// 
// 
//
// 
//
// 示例： 
//
// 
//输入
//["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
//[[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
//输出
//[null, null, null, 1, null, -1, null, -1, 3, 4]
//
//解释
//LRUCache lRUCache = new LRUCache(2);
//lRUCache.put(1, 1); // 缓存是 {1=1}
//lRUCache.put(2, 2); // 缓存是 {1=1, 2=2}
//lRUCache.get(1);    // 返回 1
//lRUCache.put(3, 3); // 该操作会使得关键字 2 作废，缓存是 {1=1, 3=3}
//lRUCache.get(2);    // 返回 -1 (未找到)
//lRUCache.put(4, 4); // 该操作会使得关键字 1 作废，缓存是 {4=4, 3=3}
//lRUCache.get(1);    // 返回 -1 (未找到)
//lRUCache.get(3);    // 返回 3
//lRUCache.get(4);    // 返回 4
// 
//
// 
//
// 提示： 
//
// 
// 1 <= capacity <= 3000 
// 0 <= key <= 10000 
// 0 <= value <= 105 
// 最多调用 2 * 105 次 get 和 put 
// 
// Related Topics 设计 哈希表 链表 双向链表 
// 👍 1940 👎 0


import java.util.*;

//leetcode submit region begin(Prohibit modification and deletion)

/**
 * 解题思路
 * 1.双向链表记录节点
 * 2.如果节点被查询，将节点放到链尾
 * 3.如果新增节点，将新增节点放到链尾
 * 4.如果有节点被移除，直接移除队首
 *
 * 解答成功:
 * 			执行耗时:58 ms,击败了13.43% 的Java用户
 * 			内存消耗:110.7 MB,击败了32.08% 的Java用户
 */
class LRUCache {

    int capacity;
    HashMap<Integer, LAUNode> map = new HashMap<>();
    LAUNode headNode = new LAUNode(-1, -1);
    LAUNode lastNode = new LAUNode(-1, -1);

    public LRUCache(int capacity) {
        this.capacity = capacity;
        headNode.next = lastNode;
        lastNode.prev = headNode;
    }
    
    public int get(int key) {
        if(map.containsKey(key)) {
            LAUNode lauNode = map.get(key);
            moveToLast(lauNode);
            return lauNode.value;
        }

        return -1;
    }
    
    public void put(int key, int value) {
        if(map.containsKey(key)) {
            LAUNode lauNode = map.get(key);
            lauNode.value = value;
            // 将节点放到队尾
            moveToLast(lauNode);
        } else {
            if(map.size() == capacity) {
                LAUNode lauNode = removeToHead();
                map.remove(lauNode.key);
            }
            LAUNode lauNode = new LAUNode(key, value);
            map.put(key, lauNode);
            // 将节点放到队尾
            putToLast(lauNode);
        }

    }



    private LAUNode removeToHead() {
        LAUNode lauNode = headNode.next;
        if(lauNode.value != -1) {
            headNode.next = headNode.next.next;
            lauNode.next.prev = headNode;
        }
        return lauNode;
    }

    private void putToLast(LAUNode lauNode) {
        lastNode.prev.next = lauNode;
        lauNode.prev = lastNode.prev;
        lauNode.next = lastNode;
        lastNode.prev = lauNode;
    }

    private void moveToLast(LAUNode lauNode) {
        lauNode.prev.next = lauNode.next;
        lauNode.next.prev = lauNode.prev;

        putToLast(lauNode);
    }
}

class LAUNode {
    Integer key;
    Integer value;

    LAUNode prev;
    LAUNode next;

    public LAUNode(Integer key, Integer value) {
        this.key = key;
        this.value = value;
    }


}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */
//leetcode submit region end(Prohibit modification and deletion)
