//给定两个数组 nums1 和 nums2 ，返回 它们的 交集 。输出结果中的每个元素一定是 唯一 的。我们可以 不考虑输出结果的顺序 。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums1 = [1,2,2,1], nums2 = [2,2]
//输出：[2]
// 
//
// 示例 2： 
//
// 
//输入：nums1 = [4,9,5], nums2 = [9,4,9,8,4]
//输出：[9,4]
//解释：[4,9] 也是可通过的
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums1.length, nums2.length <= 1000 
// 0 <= nums1[i], nums2[i] <= 1000 
// 
//
// Related Topics 数组 哈希表 双指针 二分查找 排序 👍 930 👎 0


import java.util.HashSet;
import java.util.Set;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution349 {
    public int[] intersection(int[] nums1, int[] nums2) {
        int[] tempArr = new int[1001];
        for (int i : nums1) {
            tempArr[i]++;
        }
        int len=0;
        for (int i : nums2) {
            if(tempArr[i] > 0) {
                tempArr[i] = -1;
                len++;
            }
        }
        int[] res = new int[len];
        int r=0;
        for (int i = 0; i < tempArr.length; i++) {
            if(tempArr[i] == -1) {
                res[r++] = i;
            }
        }
        return res;
    }

    public int[] intersection1(int[] nums1, int[] nums2) {


        Set<Integer> tempSet = new HashSet<>();
        for (int longNum : nums1) {
            tempSet.add(longNum);
        }

        Set<Integer> resultSet = new HashSet<>();
        for (int num : nums2) {
            if(tempSet.contains(num)) {
                resultSet.add(num);
            }
        }

        int[] resultArr = new int[resultSet.size()];
        int i=0;
        for (Integer res : resultSet) {
            resultArr[i++] = res;
        }
        return resultArr;


    }
}
//leetcode submit region end(Prohibit modification and deletion)
