//给你两个按 非递减顺序 排列的整数数组 nums1 和 nums2，另有两个整数 m 和 n ，分别表示 nums1 和 nums2 中的元素数目。 
//
// 请你 合并 nums2 到 nums1 中，使合并后的数组同样按 非递减顺序 排列。 
//
// 注意：最终，合并后数组不应由函数返回，而是存储在数组 nums1 中。为了应对这种情况，nums1 的初始长度为 m + n，其中前 m 个元素表示应合并
//的元素，后 n 个元素为 0 ，应忽略。nums2 的长度为 n 。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
//输出：[1,2,2,3,5,6]
//解释：需要合并 [1,2,3] 和 [2,5,6] 。
//合并结果是 [1,2,2,3,5,6] ，其中斜体加粗标注的为 nums1 中的元素。
// 
//
// 示例 2： 
//
// 
//输入：nums1 = [1], m = 1, nums2 = [], n = 0
//输出：[1]
//解释：需要合并 [1] 和 [] 。
//合并结果是 [1] 。
// 
//
// 示例 3： 
//
// 
//输入：nums1 = [0], m = 0, nums2 = [1], n = 1
//输出：[1]
//解释：需要合并的数组是 [] 和 [1] 。
//合并结果是 [1] 。
//注意，因为 m = 0 ，所以 nums1 中没有元素。nums1 中仅存的 0 仅仅是为了确保合并结果可以顺利存放到 nums1 中。
// 
//
// 
//
// 提示： 
//
// 
// nums1.length == m + n 
// nums2.length == n 
// 0 <= m, n <= 200 
// 1 <= m + n <= 200 
// -109 <= nums1[i], nums2[j] <= 109 
// 
//
// 
//
// 进阶：你可以设计实现一个时间复杂度为 O(m + n) 的算法解决此问题吗？ 
// Related Topics 数组 双指针 排序 
// 👍 1284 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution88 {
    /**
     * 解题思路：
     * 1.现将两个数组合并到一个数组
     * 2.利用数组归并排序(已知左右是有序的)
     *
     * 解答成功:
     * 			执行耗时:0 ms,击败了100.00% 的Java用户
     * 			内存消耗:41.3 MB,击败了8.35% 的Java用户
     * @param nums1
     * @param m
     * @param nums2
     * @param n
     */
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        // 合并数组
        for(int i=0; i<n; i++) {
            nums1[m+i] = nums2[i];
        }
        if(nums1.length == 0) return;

        // 归并排序
        int left = 0;
        int right = nums1.length - 1;
        int[] tmp = new int[nums1.length];
        merge(nums1, left, m-1, right, tmp);

    }

    private void merge(int[] nums1, int left, int mid, int right, int[] tmp) {
        int l=left, r=mid+1, t=0;
        while(l <=mid && r<=right) {
            if(nums1[l] <= nums1[r]) {
                tmp[t] = nums1[l];
                l++;
            } else {
                tmp[t] = nums1[r];
                r++;
            }
            t++;
        }
        while(l<=mid){
            tmp[t] = nums1[l];
            l++;
            t++;
        }
        while(r<=right){
            tmp[t] = nums1[r];
            r++;
            t++;
        }

        // 将tmp回写到nums1中
        t=0;
        int nums1Left = left;
        while(nums1Left <= right) {
            nums1[nums1Left] = tmp[t];
            t++;
            nums1Left++;
        }
    }

    public static void main(String[] args) {
        new Solution88().merge(new int[]{1,2,3,0,0,0}, 3, new int[]{2,5,6},3);
    }
}
//leetcode submit region end(Prohibit modification and deletion)
