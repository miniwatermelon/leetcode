//给你一个二叉树的根节点 root ，按 任意顺序 ，返回所有从根节点到叶子节点的路径。 
//
// 叶子节点 是指没有子节点的节点。 
//
// 示例 1： 
// 
// 
//输入：root = [1,2,3,null,5]
//输出：["1->2->5","1->3"]
// 
//
// 示例 2： 
//
// 
//输入：root = [1]
//输出：["1"]
// 
//
// 
//
// 提示： 
//
// 
// 树中节点的数目在范围 [1, 100] 内 
// -100 <= Node.val <= 100 
// 
//
// Related Topics 树 深度优先搜索 字符串 回溯 二叉树 👍 1201 👎 0


//leetcode submit region begin(Prohibit modification and deletion)

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution257 {
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> resultList = new ArrayList<>();
        Stack<String> treePath = new Stack<>();

        deepTree(root, treePath, resultList);

        return resultList;
    }

    private void deepTree(TreeNode treeNode, Stack<String> treePath, List<String> resultList) {
        treePath.push(treeNode.val + "");
        if(treeNode.left != null) {
            deepTree(treeNode.left ,treePath, resultList);
        }
        if(treeNode.right != null) {
            deepTree(treeNode.right ,treePath, resultList);
        }
        if(treeNode.left == null && treeNode.right == null) {
            // 收集结束
            convertList(treePath, resultList);
        }
        // 退格
        treePath.pop();
    }

    private void convertList(Stack<String> treePath, List<String> resultList) {
        String[] array = treePath.toArray(new String[0]);
        String result = String.join("->", array);
        resultList.add(result);
    }
}
//leetcode submit region end(Prohibit modification and deletion)
