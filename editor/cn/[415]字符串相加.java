//给定两个字符串形式的非负整数 num1 和num2 ，计算它们的和并同样以字符串形式返回。 
//
// 你不能使用任何內建的用于处理大整数的库（比如 BigInteger）， 也不能直接将输入的字符串转换为整数形式。 
//
// 
//
// 示例 1： 
//
// 
//输入：num1 = "11", num2 = "123"
//输出："134"
// 
//
// 示例 2： 
//
// 
//输入：num1 = "456", num2 = "77"
//输出："533"
// 
//
// 示例 3： 
//
// 
//输入：num1 = "0", num2 = "0"
//输出："0"
// 
//
// 
//
// 
//
// 提示： 
//
// 
// 1 <= num1.length, num2.length <= 10⁴ 
// num1 和num2 都只包含数字 0-9 
// num1 和num2 都不包含任何前导零 
// 
//
// Related Topics 数学 字符串 模拟 👍 848 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution415 {
    public String addStrings(String num1, String num2) {
        int len = num1.length() > num2.length() ? num1.length() + 1 : num2.length() + 1;
        char[] arr = new char[len];
        char[] num1Arr = num1.toCharArray();
        char[] num2Arr = num2.toCharArray();
        if(num1Arr.length > num2Arr.length) {
            add(arr, num1Arr, num2Arr);
        }else {
            add(arr, num2Arr, num1Arr);
        }
        if(arr[0] != '0') {
            return new String(arr);
        }
        return new String(arr, 1, arr.length - 1);
    }
    private void add(char[] arr, char[] longArr, char[] shortArr) {
        int longIndex = longArr.length - 1;
        int shortIndex = shortArr.length - 1;
        int arrIdx = arr.length - 1;
        int more = 0;
        while(shortIndex >= 0) {
            int val1 = longArr[longIndex--] - '0';
            int val2 = shortArr[shortIndex--] - '0';
            int arrVal = val1 + val2 + more;
            if(arrVal >= 10) {
                arrVal -= 10;
                more = 1;
            } else {
                more = 0;
            }
            arr[arrIdx --] = (char) (arrVal + '0');
        }

        while(longIndex >= 0) {
            int val1 = longArr[longIndex--] - '0';
            int arrVal = val1 + more;
            if(arrVal >= 10) {
                arrVal -= 10;
                more = 1;
            } else {
                more = 0;
            }
            arr[arrIdx --] = (char) (arrVal + '0');
        }

        arr[0] = (char) (more + '0');
    }

    public static void main(String[] args) {
//        System.out.println(new Solution415().addStrings("11", "123"));
//        char val1 = '1', var2='2';
//        int var3 = val1 - '0' + var2 - '0';
//        System.out.println(var3);
//        System.out.println((char)(var3 + '0'));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
