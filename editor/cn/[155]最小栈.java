//设计一个支持 push ，pop ，top 操作，并能在常数时间内检索到最小元素的栈。 
//
// 
// push(x) —— 将元素 x 推入栈中。 
// pop() —— 删除栈顶的元素。 
// top() —— 获取栈顶元素。 
// getMin() —— 检索栈中的最小元素。 
// 
//
// 
//
// 示例: 
//
// 输入：
//["MinStack","push","push","push","getMin","pop","top","getMin"]
//[[],[-2],[0],[-3],[],[],[],[]]
//
//输出：
//[null,null,null,null,-3,null,0,-2]
//
//解释：
//MinStack minStack = new MinStack();
//minStack.push(-2);
//minStack.push(0);
//minStack.push(-3);
//minStack.getMin();   --> 返回 -3.
//minStack.pop();
//minStack.top();      --> 返回 0.
//minStack.getMin();   --> 返回 -2.
// 
//
// 
//
// 提示： 
//
// 
// pop、top 和 getMin 操作总是在 非空栈 上调用。 
// 
// Related Topics 栈 设计 
// 👍 1169 👎 0


import java.util.ArrayList;
import java.util.List;

//leetcode submit region begin(Prohibit modification and deletion)

/**
 * 解答成功:
 * 			执行耗时:4 ms,击败了98.04% 的Java用户
 * 			内存消耗:43.2 MB,击败了8.08% 的Java用户
 */
class MinStack {

    int min = Integer.MAX_VALUE;
    List<Integer> arr = new ArrayList<>();
    int index = -1;

    public MinStack() {

    }
    
    public void push(int val) {
        if(min > val) {
            min = val;
        }
        arr.add(val);
        index++;
    }
    
    public void pop() {
        int val = arr.remove(index);
        if(min == val) {
            min = Integer.MAX_VALUE;
            for (Integer integer : arr) {
                if(min > integer) {
                    min = integer;
                }
            }
        }
        index--;
    }
    
    public int top() {
        return arr.get(index);
    }
    
    public int getMin() {
        return min;
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(val);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */
//leetcode submit region end(Prohibit modification and deletion)
