//给你链表的头节点 head ，每 k 个节点一组进行翻转，请你返回修改后的链表。 
//
// k 是一个正整数，它的值小于或等于链表的长度。如果节点总数不是 k 的整数倍，那么请将最后剩余的节点保持原有顺序。 
//
// 你不能只是单纯的改变节点内部的值，而是需要实际进行节点交换。 
//
// 
//
// 示例 1： 
// 
// 
//输入：head = [1,2,3,4,5], k = 2
//输出：[2,1,4,3,5]
// 
//
// 示例 2： 
//
// 
//
// 
//输入：head = [1,2,3,4,5], k = 3
//输出：[3,2,1,4,5]
// 
//
// 
//提示：
//
// 
// 链表中的节点数目为 n 
// 1 <= k <= n <= 5000 
// 0 <= Node.val <= 1000 
// 
//
// 
//
// 进阶：你可以设计一个只用 O(1) 额外内存空间的算法解决此问题吗？ 
//
// 
// 
//
// Related Topics 递归 链表 👍 2376 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution25 {
    public ListNode reverseKGroup(ListNode head, int k) {

        ListNode[] arr = new ListNode[k+2];

        boolean first = true;
        int loop = 1;
        while(head != null) {
            arr[loop+1] = new ListNode(head.val);
            if(loop == k) {
                ListNode node = arr[k+1];
                ListNode curNode = node;
                arr[k+1] = null;
                for(int i=k; i>1; i--) {
                    ListNode nextNode = arr[i];
                    curNode.next = nextNode;
                    curNode = nextNode;
                    arr[i] = null;
                }
                if(arr[1] != null) {
                    arr[1].next = node;
                }
                arr[1] = curNode;

                if(first) {
                    arr[0] = node;
                    first = false;
                }

                loop = 1;
            } else {
                loop++;
            }
            head = head.next;

        }

        ListNode curNode = arr[1];
        for(int i=2; i<k+2; i++) {
            if(arr[i] != null) {
                ListNode nextNode = arr[i];
                curNode.next = nextNode;
                curNode = nextNode;
            }
        }

        return arr[0];
    }

    public static void main(String[] args) {
        ListNode rootNode = new ListNode(1,
                new ListNode(2,
                        new ListNode(3,
                                new ListNode(4,
                                        new ListNode(5)))));
        new Solution25().reverseKGroup(rootNode, 2);
    }
}
//leetcode submit region end(Prohibit modification and deletion)
