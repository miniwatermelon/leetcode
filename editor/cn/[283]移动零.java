//给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。 
//
// 请注意 ，必须在不复制数组的情况下原地对数组进行操作。 
//
// 
//
// 示例 1: 
//
// 
//输入: nums = [0,1,0,3,12]
//输出: [1,3,12,0,0]
// 
//
// 示例 2: 
//
// 
//输入: nums = [0]
//输出: [0] 
//
// 
//
// 提示: 
// 
//
// 
// 1 <= nums.length <= 104 
// -231 <= nums[i] <= 231 - 1 
// 
//
// 
//
// 进阶：你能尽量减少完成的操作次数吗？ 
// Related Topics 数组 双指针 
// 👍 1456 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution283 {

    public void moveZeroes(int[] nums) {
        int n = nums.length, left = 0, right = 0;
        while (right < n) {
            if (nums[right] != 0) {
                swap(nums, left, right);
                left++;
            }
            right++;
        }
    }

    public void swap(int[] nums, int left, int right) {
        int temp = nums[left];
        nums[left] = nums[right];
        nums[right] = temp;
    }


    /**
     * 解题思路：
     * 找到第一个0和这个0之后的非0元素，使其交换顺序
     *
     * 解答成功:
     * 			执行耗时:32 ms,击败了10.38% 的Java用户
     * 			内存消耗:42.4 MB,击败了31.40% 的Java用户
     * @param nums
     */
    public void moveZeroes1(int[] nums) {


        for (int i = 0; i < nums.length; i++) {
            int zeroIndex = i;
            while(nums[zeroIndex] != 0) {
                zeroIndex ++;
                if(zeroIndex == nums.length) break;
            }
            if(zeroIndex == nums.length) { return;}

            int numIndex = zeroIndex;
            while(nums[numIndex] == 0) {
                numIndex++;
                if(numIndex == nums.length) break;
            }
            if(numIndex == nums.length) return;

            int temp = nums[zeroIndex];
            nums[zeroIndex] = nums[numIndex];
            nums[numIndex] = temp;

            i = zeroIndex;
        }
    }

    public static void main(String[] args) {
        new Solution283().moveZeroes(new int[]{0,1,0,3,12});
    }
}
//leetcode submit region end(Prohibit modification and deletion)
