**树的头结点root我们先放入栈Stack，然后弹出打印；
利用栈的特性：先进后出，所以先放入右子树、再放入左子树
左子树left就理解为下一个头结点、弹出left，放入left.right和left.left...**
![1644321283(1).png](https://pic.leetcode-cn.com/1644321306-bVnCju-1644321283\(1\).png)
利用栈的特性，我们实现了一个“平躺着的二叉树”，并且栈的先入后出原则也很符合前序遍历的实现
![1644321718(1).png](https://pic.leetcode-cn.com/1644321730-cYgWSZ-1644321718\(1\).png)

```java
class Solution {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        
        if(root == null){
            return list;
        }
        

        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while(!stack.isEmpty()){
            root = stack.pop();
            list.add(root.val);
            if(root.right != null){
                stack.push(root.right);
            }
            if(root.left != null){
                stack.push(root.left);
            }
                        
        }
        return list;
    }
}
```
