## DFS

数据范围只有 *20*，而且每个数据只有 *+/-* 两种选择，因此可以直接使用 DFS 进行「爆搜」。

而 DFS 有「使用全局变量维护」和「接收返回值处理」两种形式。

代码：
```Java []
class Solution {
    public int findTargetSumWays(int[] nums, int t) {
        return dfs(nums, t, 0, 0);
    }
    int dfs(int[] nums, int t, int u, int cur) {
        if (u == nums.length) {
            return cur == t ? 1 : 0;
        }
        int left = dfs(nums, t, u + 1, cur + nums[u]);
        int right = dfs(nums, t, u + 1, cur - nums[u]);
        return left + right;
    }
}
```
```Java []
class Solution {
    int ans = 0;
    public int findTargetSumWays(int[] nums, int t) {
        dfs(nums, t, 0, 0);
        return ans;
    }
    void dfs(int[] nums, int t, int u, int cur) {
        if (u == nums.length) {
            ans += cur == t ? 1 : 0;
            return;
        }
        dfs(nums, t, u + 1, cur + nums[u]);
        dfs(nums, t, u + 1, cur - nums[u]);
    }
}
```
* 时间复杂度：*O(2^n)*
* 空间复杂度：忽略递归带来的额外空间消耗。复杂度为 *O(1)*

---

## 记忆化搜索

不难发现，在 DFS 的函数签名中只有「数值下标 `u`」和「当前结算结果 `cur`」为可变参数，考虑将其作为记忆化容器的两个维度，返回值作为记忆化容器的记录值。

由于 `cur` 存在负权值，为了方便，我们这里不设计成静态数组，而是使用「哈希表」进行记录。

以上分析都在 [（题解）403. 青蛙过河](https://leetcode-cn.com/problems/frog-jump/solution/gong-shui-san-xie-yi-ti-duo-jie-jiang-di-74fw/) 完整讲过。

代码：
```Java []
class Solution {
    public int findTargetSumWays(int[] nums, int t) {
        return dfs(nums, t, 0, 0);
    }
    Map<String, Integer> cache = new HashMap<>();
    int dfs(int[] nums, int t, int u, int cur) {
        String key = u + "_" + cur;
        if (cache.containsKey(key)) return cache.get(key);
        if (u == nums.length) {
            cache.put(key, cur == t ? 1 : 0);
            return cache.get(key);
        }
        int left = dfs(nums, t, u + 1, cur + nums[u]);
        int right = dfs(nums, t, u + 1, cur - nums[u]);
        cache.put(key, left + right);
        return cache.get(key);
    }
}
```
* 时间复杂度：![O(n*\sum_{i=0}^{n-1}abs(nums\[i\])) ](./p__O_n_*_sum_{i_=_0}^{n_-_1}_abs_nums_i____.png) 
* 空间复杂度：忽略递归带来的额外空间消耗。复杂度为 ![O(n*\sum_{i=0}^{n-1}abs(nums\[i\])) ](./p__O_n_*_sum_{i_=_0}^{n_-_1}_abs_nums_i____.png) 

---

## 动态规划

能够以「递归」的形式实现动态规划（记忆化搜索），自然也能使用「递推」的方式进行实现。

根据记忆化搜索的分析，我们可以定义：

***f[i][j]* 代表考虑前 *i* 个数，当前计算结果为 *j* 的方案数，令 `nums` 下标从 *1* 开始。**

那么 *f[n][target]* 为最终答案，*f[0][0] = 1* 为初始条件：代表不考虑任何数，凑出计算结果为 *0* 的方案数为 *1* 种。

根据每个数值只能搭配 *+/-* 使用，可得状态转移方程：

*f[i][j] = f[i - 1][j - nums[i - 1]] + f[i - 1][j + nums[i - 1]]*

到这里，既有了「状态定义」和「转移方程」，又有了可以滚动下去的「有效值」（起始条件）。

距离我们完成所有分析还差最后一步。

当使用递推形式时，我们通常会使用「静态数组」来存储动规值，因此还需要考虑维度范围的：

* 第一维为物品数量：范围为 `nums` 数组长度
* 第二维为中间结果：令 `s` 为所有 `nums` 元素的总和（题目给定了 `nums[i]` 为非负数的条件，否则需要对 `nums[i]` 取绝对值再累加），那么中间结果的范围为 *[-s, s]*
    
因此，我们可以确定动规数组的大小。**同时在转移时，对第二维度的使用做一个 `s` 的右偏移，以确保「负权值」也能够被合理计算/存储。**

代码：
```Java []
class Solution {
    public int findTargetSumWays(int[] nums, int t) {
        int n = nums.length;
        int s = 0;
        for (int i : nums) s += Math.abs(i);
        if (Math.abs(t) > s) return 0;
        int[][] f = new int[n + 1][2 * s + 1];
        f[0][0 + s] = 1;
        for (int i = 1; i <= n; i++) {
            int x = nums[i - 1];
            for (int j = -s; j <= s; j++) {
                if ((j - x) + s >= 0) f[i][j + s] += f[i - 1][(j - x) + s];
                if ((j + x) + s <= 2 * s) f[i][j + s] += f[i - 1][(j + x) + s];
            }
        }
        return f[n][t + s];
    }
}
```
* 时间复杂度：![O(n*\sum_{i=0}^{n-1}abs(nums\[i\])) ](./p__O_n_*_sum_{i_=_0}^{n_-_1}_abs_nums_i____.png) 
* 空间复杂度：![O(n*\sum_{i=0}^{n-1}abs(nums\[i\])) ](./p__O_n_*_sum_{i_=_0}^{n_-_1}_abs_nums_i____.png) 

---

## 动态规划（优化）

**在上述「动态规划」分析中，我们总是尝试将所有的状态值都计算出来，当中包含很多对「目标状态」不可达的“额外”状态值。**

即达成某些状态后，不可能再回到我们的「目标状态」。

例如当我们的 *target* 不为 *-s* 和 *s* 时，*-s* 和 *s* 就是两个对「目标状态」不可达的“额外”状态值，到达 *-s* 或 *s* 已经使用所有数值，对 *target* 不可达。

那么我们如何规避掉这些“额外”状态值呢？

我们可以从哪些数值使用哪种符号来分析，即划分为「负值部分」&「非负值部分」，令「负值部分」的绝对值总和为 *m*，即可得：

*(s - m) - m = s - 2 * m = target*

变形得：

![m=\frac{s-target}{2} ](./p__m_=_frac{s_-_target}{2}_.png) 

问题转换为：**只使用 *+* 运算符，从 `nums` 凑出 *m* 的方案数。**

**这样「原问题的具体方案」和「转换问题的具体方案」具有一一对应关系：「转换问题」中凑出来的数值部分在实际计算中应用 *-*，剩余部分应用 *+*，从而实现凑出来原问题的 *target* 值。**

另外，由于 `nums` 均为非负整数，因此我们需要确保 *s - target* 能够被 *2* 整除。

同时，由于问题转换为 **从 `nums` 中凑出 *m* 的方案数，因此「状态定义」和「状态转移」都需要进行调整（01 背包求方案数）：**

**定义 *f[i][j]* 为从 `nums` 凑出总和「恰好」为 *j* 的方案数。**

最终答案为 *f[n][m]*，*f[0][0] = 1* 为起始条件：代表不考虑任何数，凑出计算结果为 *0* 的方案数为 *1* 种。

每个数值有「选」和「不选」两种决策，转移方程为：

*f[i][j] = f[i - 1][j] + f[i - 1][j - nums[i - 1]]*

代码：
```Java []
class Solution {
    public int findTargetSumWays(int[] nums, int t) {
        int n = nums.length;
        int s = 0;
        for (int i : nums) s += Math.abs(i);
        if (t > s || (s - t) % 2 != 0) return 0;
        int m = (s - t) / 2;
        int[][] f = new int[n + 1][m + 1];
        f[0][0] = 1;
        for (int i = 1; i <= n; i++) {
            int x = nums[i - 1];
            for (int j = 0; j <= m; j++) {
                f[i][j] += f[i - 1][j];
                if (j >= x) f[i][j] += f[i - 1][j - x];
            }
        }
        return f[n][m];
    }
}
```
* 时间复杂度：![O(n*((\sum_{i=0}^{n-1}abs(nums\[i\]))-target)) ](./p__O_n_*___sum_{i_=_0}^{n_-_1}_abs_nums_i____-_target___.png) 
* 空间复杂度：![O(n*((\sum_{i=0}^{n-1}abs(nums\[i\]))-target)) ](./p__O_n_*___sum_{i_=_0}^{n_-_1}_abs_nums_i____-_target___.png) 

---

## 背包问题（目录）

1. 01背包 : [背包问题 第一讲](https://mp.weixin.qq.com/s/xmgK7SrTnFIM3Owpk-emmg)

    1. 【练习】01背包 : [背包问题 第二讲（416. 分割等和子集）](https://leetcode-cn.com/problems/partition-equal-subset-sum/solution/gong-shui-san-xie-bei-bao-wen-ti-shang-r-ln14/)
    
    2. 【学习&练习】01背包 : [背包问题 第三讲（416. 分割等和子集）](https://leetcode-cn.com/problems/partition-equal-subset-sum/solution/gong-shui-san-xie-bei-bao-wen-ti-xia-con-mr8a/)

2. 完全背包 : [背包问题 第四讲](https://mp.weixin.qq.com/s?__biz=MzU4NDE3MTEyMA==&mid=2247486107&idx=1&sn=e5fa523008fc5588737b7ed801caf4c3&chksm=fd9ca184caeb28926959c0987208a3932ed9c965267ed366b5b82a6fc16d42f1ff40c29db5f1&token=990510480&lang=zh_CN#rd)

    1. 【练习】完全背包 : [背包问题 第五讲（279. 完全平方数）](https://leetcode-cn.com/problems/perfect-squares/solution/gong-shui-san-xie-xiang-jie-wan-quan-bei-nqes/)
    
    2. 【练习】完全背包 : [背包问题 第六讲（322. 零钱兑换）](https://leetcode-cn.com/problems/coin-change/solution/dong-tai-gui-hua-bei-bao-wen-ti-zhan-zai-3265/)
    
    3. 【练习】完全背包 : [背包问题 第七讲（518. 零钱兑换 II）](https://leetcode-cn.com/problems/coin-change-2/solution/gong-shui-san-xie-xiang-jie-wan-quan-bei-6hxv/)

3. 多重背包 : [背包问题 第八讲](https://mp.weixin.qq.com/s?__biz=MzU4NDE3MTEyMA==&mid=2247486649&idx=1&sn=ba09ee2d78377c2ddbb9e43622880133&chksm=fd9ca7a6caeb2eb0db61b7604a4aaa8d3ca90d6bc05eb6f50aaab415c4bd7f0047c1ca591018&token=1008907671&lang=zh_CN&scene=21#wechat_redirect)


4. 多重背包（优化篇）

    1. 多重背包（优化篇）: [背包问题 第九讲](https://mp.weixin.qq.com/s?__biz=MzU4NDE3MTEyMA==&mid=2247486796&idx=1&sn=a382b38f8aed295410550bb1767437bd&chksm=fd9ca653caeb2f456262bbf70ffe1eeda8758b426a901a6ac15be184e7017870020e456c6fa2&token=1821593597&lang=zh_CN#rd)
    
    2. 多重背包（优化篇）: [背包问题 第十讲](https://mp.weixin.qq.com/s?__biz=MzU4NDE3MTEyMA==&mid=2247486963&idx=1&sn=51d34f0f841122ed9be2335a402041e8&chksm=fd9ca6eccaeb2ffa1abe413177be376799b427b092bfb73c13e7b77e171b460f4c24b3b7d3bc&token=1821593597&lang=zh_CN#rd)

5. 混合背包

    1. 【练习】混合背包

6. 分组背包

    1. 【练习】分组背包

7. 多维背包

    1. 【练习】多维背包 : [背包问题 第 * 讲（474. 一和零）](https://leetcode-cn.com/problems/ones-and-zeroes/solution/gong-shui-san-xie-xiang-jie-ru-he-zhuan-174wv/)
    2. 【练习】多维背包 : [背包问题 第 * 讲（879. 盈利计划）](https://leetcode-cn.com/problems/profitable-schemes/solution/gong-shui-san-xie-te-shu-duo-wei-fei-yon-7su9/)


8. 树形背包

    1. 【练习】树形背包

9. 背包求方案数

    1. 【练习】背包求方案数 : [背包问题 第 * 讲（494. 目标和）](https://leetcode-cn.com/problems/target-sum/solution/gong-shui-san-xie-yi-ti-si-jie-dfs-ji-yi-et5b/)
    2. 【练习】背包求方案数 : [背包问题 第 * 讲（879. 盈利计划）](https://leetcode-cn.com/problems/profitable-schemes/solution/gong-shui-san-xie-te-shu-duo-wei-fei-yon-7su9/)


10. 背包求具体方案

    1. 【练习】背包求具体方案 : [背包问题 第 * 讲（1049. 最后一块石头的重量 II）](https://leetcode-cn.com/problems/last-stone-weight-ii/solution/gong-shui-san-xie-xiang-jie-wei-he-neng-jgxik/)

11. 泛化背包

    1. 【练习】泛化背包


---

## 最后

**如果有帮助到你，请给题解点个赞和收藏，让更多的人看到 ~ ("▔□▔)/**

也欢迎你 [关注我](https://oscimg.oschina.net/oscnet/up-19688dc1af05cf8bdea43b2a863038ab9e5.png)（公主号后台回复「送书」即可参与长期看题解学算法送实体书活动）或 加入[「组队打卡」](https://leetcode-cn.com/u/ac_oier/)小群 ，提供写「证明」&「思路」的高质量题解。

所有题解已经加入 [刷题指南](https://github.com/SharingSource/LogicStack-LeetCode/wiki)，欢迎 star 哦 ~ 