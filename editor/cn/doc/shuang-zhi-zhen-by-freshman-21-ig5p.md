最重要的是sum的值实时更新
```
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        //java.util.Arrays.ArrayList
        List<List<Integer>> ans = new ArrayList();
        int len = nums.length;
        if(nums.length<3)return ans;
        Arrays.sort(nums);
        for(int i=0;i<len;i++){
            if(nums[i] > 0) break; // 如果当前数字大于0，则三数之和一定大于0，所以结束循环
            if(i > 0 && nums[i] == nums[i-1]) continue; // 去重
            int l=i+1,r=len-1;
            while(l<r){//不断从左 或 从右逼近 顺序不一定
            int sum = nums[l]+nums[i]+nums[r];//fixed "i"
                if(sum==0){
                    ans.add(Arrays.asList(nums[r],nums[l],nums[i]));
                    while (l<r && nums[l] == nums[l+1]) l++; // 去重
                    while (l<r && nums[r] == nums[r-1]) r--; // 去重
                    l++;
                    r--;
                }
                if(sum<0 ){
                    l++;
                }
                if(sum>0 ){
                    r--;
                }
                //刷新sum值
             }
        
        }return ans;
    }
}
```
