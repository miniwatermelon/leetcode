每一轮搜索，不能往前搜，否则会产生重复解。



```
import java.util.ArrayList;
import java.util.List;

class Solution {
    static List<List<Integer>> ret;
    public static void dfs (int[] candidates,int start, List<Integer> nums, int cur, int target) {
        if (cur == target) {
            ret.add(new ArrayList<>(nums));
            return;
        }

        for (int i = start; i < candidates.length; i++) {
            if (cur + candidates[i] > target) continue;
            cur += candidates[i];
            nums.add(candidates[i]);
            dfs(candidates, i, nums, cur, target);
            nums.remove(nums.size() - 1);
            cur -= candidates[i];
        }
    }
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        ret = new ArrayList<>();
        dfs(candidates, 0, new ArrayList<>(), 0, target);
        return ret;
    }
}
```
