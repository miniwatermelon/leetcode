left为数字0的右开边界，right为数字2的左开边界。

```
00001102001112222
    l       r 
```

* 当把i交换到左边界的时候：i可以直接+1推进，因为左边不会有数字2了。
* 当把i交换到右边界的时候：可能会把数字2或者数字0置换i位置上，所以我们不能推进i。

如果你还是对i的变化感到迷惑，那么下面是更加详细的解释：

i位置上的数字代表着我们当前需要处理的数字。当i为数字1的时候，我们什么都不需要做，直接+1即可。如果是0，我们放到左边，如果是2，放到右边。

right指针上的数字交换到i位置上以后，我们还需要对该数字进行处理。这个数字是一个全新的(0,1,2都有可能)，没有处理过的数字。

如果交换过来数字0或者数字2了，我们推进了i的话，那么就会出错。

而需要交换i位置数字到left上时，那么i位置上的数字一定是0。我们可以分成两种情况来讨论：
1. left和i和重合：那么就相当于i和i自己交换，此时i位置上的数字就处理完了，i和left都可以推进。
2. i和left不重合：那么left对应的数字一定是1，如果不是1的话，left和i一定是重合的（包含一种特殊情况，数组中只有0和2的时候）。此时交换完以后，i位置上就会是1，那么也可以直接+1推进。

```java
class Solution {
    public void sortColors(int[] nums) {
        int left=0,right=nums.length-1;
        int i=0;
        while(i<=right){
            if(nums[i]==0)
                swap(nums,i++,left++);
            else if(nums[i]==2)
                swap(nums,i,right--);
            else
                i++;
        }
    }
    private void swap(int[] nums,int i,int j){
        int temp=nums[i];
        nums[i]=nums[j];
        nums[j]=temp;
    }
} 
```
