```java
class Solution {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> list=new ArrayList<>();
        int n=nums.length;
        for(int i=0;i<nums.length;i++){
            int j=(nums[i]-1)%n;//加上n之后 依然是对应位置 取模还原他本来的值	
            nums[j]+=n;//对应位置加n
        }
        for(int i=0;i<nums.length;i++){
            if(nums[i]<=n)list.add(i+1);//数组下标从0开始 记得加一
        }
        return list;
    }
}
```