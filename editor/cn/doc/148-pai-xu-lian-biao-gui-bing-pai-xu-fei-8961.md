归并排序，非递归
```
public ListNode sortList(ListNode head) {
        if(head == null || head.next == null){
            return head;
        }

        int n = 0;
        ListNode curr = head;
        while(curr != null){
            n++;
            curr = curr.next;
        }

        // i <<= 1 替换i += i，提高效率
        for(int i = 1; i < n ; i<<=1){
            // 迭代启动项
            ListNode l1 = head, l2 = null;
            while(true) {
                // 定位l2
                curr = l1;
                int j = 1;
                while(curr != null && j <= i){
                    j++;
                    curr = curr.next;
                }
                if(j > i){
                    l2 = curr;
                } else {
                    // 说明没有了
                    l2 = null;
                }

                if(l2 == null){
                    break;
                }

                // 这里顺序不能乱，一定要放在doMerge之前
                j = 1;
                while(curr != null && j <= i){
                    j++;
                    curr = curr.next;
                }

                // 归并
                doMerge(l1, l2, i);
                
                // 重新定位l1
                l1 = curr;
            } 
        }

        return head;
    }

    public static void doMerge(ListNode l1, ListNode l2, int length){
        // 复制一个头结点，用来雀占鸠巢
        ListNode l11 = new ListNode();
        l11.val = l1.val;
        l11.next = l1.next;

        ListNode t1 = new ListNode(), t2 = l11, t3 = l2, r = t1;
        while(t2 != null && t3 != null && t2 != l2 && length > 0){
            if(t2.val < t3.val){
                // 使用t2
                t1.next = t2;
                t2 = t2.next;
            } else {
                // 使用t3
                t1.next = t3;
                t3 = t3.next;
                length--;
            }

            t1 = t1.next;
        }

        while(t2 != null && t2 != l2){
            // 使用t2
            t1.next = t2;
            t1 = t2;
            t2 = t2.next;
        }

        // 首元结点，雀占鸠巢
        l1.val = r.next.val;
        l1.next = r.next.next;

        // 尾元结点，直接使用t3即可，即便l2还有未遍历完的
        t1.next = t3;
    }

}
```
