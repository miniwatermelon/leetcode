
### 代码

```java
class Solution {
    public boolean canJump(int[] nums) {
        // 1.做完看了评论后
        int len = nums.length;
        int max_get = 0;
        for(int i = 0;i<len;++i){
            if(i<=max_get){
                max_get = Math.max(max_get,i+nums[i]);
                if(max_get>=len-1){
                    return true;
                }
            }
        }
        return false;

        //2.我做的方法，使用队列来做，就是效率有点低！
        // int len = nums.length;
        // if(len==1)return true;
        // Deque<Integer> queue = new LinkedList<>();
        // HashSet<Integer> visted  = new HashSet<>();
        // queue.offer(0);
        // visted.add(0);
        // while(!queue.isEmpty()){
        //     int begin = queue.poll();
        //     int step = nums[begin];
        //     for(int i = 1;i<=step;++i){
        //         if(begin+i==len-1){
        //             return true;
        //         }
        //         if(visted.add(begin+i)){
        //          queue.offer(begin+i);
        //         }
        //     }
        // }
        // return false;
    }
}
```