### 解题思路
贪心算法！！

### 代码

```java
class Solution {
    public boolean canJump(int[] nums) {
        int len = nums.length-1;
        int maxDis = nums[0];
        if (len==0){
            return true;
        }
        for (int i = 1; i < nums.length; i++) {
            if (maxDis >= i){
                int maxTmpDis = i + nums[i];
                if (maxTmpDis >= len){
                    return true;
                }
                if (maxTmpDis>maxDis){
                    maxDis = maxTmpDis;
                }
            }
        }
        return false;
    }
}
```