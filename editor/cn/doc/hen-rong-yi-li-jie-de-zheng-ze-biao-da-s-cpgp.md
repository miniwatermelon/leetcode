### 说明：
* 是自己做题的时候的一些思考和技巧，如果有不对的地方，欢迎大家批评指正！
* 小白也能全部看得懂的思路，看不懂你来K我！

首先，我们知道这题可以用动态规划来解决，那么我们按照动态规划的步骤一步步来：
我以s="aab",p="c * a * b"来说明整个的过程

### 1.定义状态
* 定义动态数组dp[m][n],(n为字符串p的长度+1,m为字符串s的长度+1)，
Q：思考为什么要 **+1**呢？
A：因为我们还要处理空字符串的情况，比如说p为空,s为空时什么情况；或者p为空,s不为空时什么情况？
* 那么dp[m][n]的含义是：p的前[n-1]个字符能否匹配s的前[m-1]个字符
Q：思考为什么是**n-1**和**m-1**?
A：因为动态数组里面加了一列和一行空字符串的匹配情况，故需要-1才能对应相应的字符串！比如说dp[1][1]是看s[0]和p[0]能否匹配

因此，创建好的dp数组应该如下图所示：
![9VDOS)BMV\[02F*IS*K7(U}C.png](https://pic.leetcode-cn.com/1615540345-WzfJtQ-9VDOS\)BMV%5B02F*IS*K7\(U%7DC.png)


### 2.确定动态转移方程
**说明：** 为了区别dp数组与字符串索引的区别(因为相差1),我们设i=r-1,j=c-1(r为dp里面的行索引,c为dp里面的列索引)

有以下几种情况是需要我们处理的：

* 当 s[i] = p[j] || p[j] == '.'   (即正好能够匹配或者相对应的是一个 **.**)

那么我们只需要看一下前面的dp[r-1][c-1]的状态，dp[r][c]继续延续即可！
即动态转移方程为：`dp[r][c]=dp[r-1][c-1]`

![G5M261P6N2Q}YBZR\]B}M9*0.png](https://pic.leetcode-cn.com/1615540638-KrlFwR-G5M261P6N2Q%7DYBZR%5DB%7DM9*0.png)

- 当 p[j] == '*'   (即匹配到了万能字符 *) ;还需要区分两种情况

① p[j-1] == s[i] ||  p[j-1] == '.'
两种情况分别对应的处理方式为：如果*的前一个字符正好对应了s,状态转移过程为:
`dp[r][c]=dp[r-1][c]`

如果是 * 的前一个字符为 . 那我们只需要看 . 的前面字符匹配情况，状态转移过程为：
`dp[r][c]=dp[r][c-2]`

![I0Q}X0ECU%AB8GKB7_NZ3A1.png](https://pic.leetcode-cn.com/1615541165-BNdxhy-I0Q%7DX0ECU%25AB8GKB7_NZ3A1.png)

![}9H@`R\]MIHY\]9KUJZG0XJ`Q.png](https://pic.leetcode-cn.com/1615541372-qqXgkH-%7D9H@%60R%5DMIHY%5D9KUJZG0XJ%60Q.png)

② 其他情况:p[j-1] 不是s[i]或者.
那么动态转移方程为: `d[r][c] = dp[r][c-2]`

![*49*I`46B}}K45U}S)\]NNSF.png](https://pic.leetcode-cn.com/1615542002-JbqOEq-*49*I%6046B%7D%7DK45U%7DS\)%5DNNSF.png)



### 3.确定边界条件
* 首先我们要确定dp[0][0],当p为空,s为空时，肯定是匹配成功的！
那么 `dp[0][0]=true`

* 当p为空字符串，而s 不为空时，dp数组必定为False，正好初始化dp数组的时候设置的是Fasle；即dp数组的第一列为False可以确定

* 当s为空字符串，而p不为空时，我们无需判断p里面的第一个值是否为"*"，如果为"*",那肯定匹配不到为Fasle,原数组正好是Fasle，所以直接从2开始判断即可。如果遇到了*,只要判断其对应的前面两个元素的dp值

初始化雨后的dp数组为：

![8CPABTUT0D~`VF\]Q32((7}6.png](https://pic.leetcode-cn.com/1615541720-ialEzY-8CPABTUT0D~%60VF%5DQ32\(\(7%7D6.png)


### 4.最终的结果
显然最终的结果是dp[m-1][n-1]的bool值

最后的dp数组为：

![77LSF~6@9V5LPS{RI6HR)0L.png](https://pic.leetcode-cn.com/1615541867-smlvuv-77LSF~6@9V5LPS%7BRI6HR\)0L.png)

```Java []
class Solution {
    public boolean isMatch(String s, String p) {
        if (p==null){
            if (s==null){
                return true;
            }else{
                return false;
            }
        }

        if (s==null && p.length()==1){
            return false;
        }

        int m = s.length()+1;
        int n = p.length()+1;

        boolean[][]dp = new boolean[m][n];

        dp[0][0] = true;

        for (int j=2;j<n;j++){
            if (p.charAt(j-1)=='*'){
                dp[0][j] = dp[0][j-2];
            }
        }

        for (int r=1;r<m;r++){
            int i = r-1;
            for (int c=1;c<n;c++){
                int j = c-1;
                if (s.charAt(i)==p.charAt(j) || p.charAt(j)=='.'){
                    dp[r][c] = dp[r-1][c-1];
                }else if (p.charAt(j)=='*'){
                    if (p.charAt(j-1)==s.charAt(i) || p.charAt(j-1)=='.'){
                        dp[r][c] = dp[r-1][c] || dp[r][c-2];
                    }else{
                        dp[r][c] = dp[r][c-2];
                    }
                }else{
                    dp[r][c] = false;
                }

            }
        }
        return dp[m-1][n-1];
    }
}
```
```python []
class Solution:
    def isMatch(self, s: str, p: str):
        if not p: return not s
        if not s and len(p) == 1: return False

        m = len(s) + 1
        n = len(p) + 1

        dp = [[False for _ in range(n)] for _ in range(m)]

        dp[0][0] = True

        # 确定dp数组的第一行，如果遇到了*,只要判断其对应的前面两个元素的dp值
        # 注意：我们无需判断p里面的第一个值是否为"*"，如果为"*",那肯定匹配不到为Fasle,原数组正好是Fasle，所以直接从2开始判断即可
        for j in range(2, n):
            if p[j-1] == '*':
                dp[0][j] = dp[0][j - 2]

        for r in range(1, m):
            i = r - 1  # 对应s中的元素
            for c in range(1, n):
                j = c - 1  # 对应p中的元素
                if s[i] == p[j] or p[j] == '.':
                    dp[r][c] = dp[r - 1][c - 1]
                elif p[j] == '*':
                    if p[j - 1] == s[i] or p[j - 1] == '.':
                        dp[r][c] = dp[r - 1][c] or dp[r][c - 2]
                    else:
                        dp[r][c] = dp[r][c - 2]
                else:
                    dp[r][c] = False

        return dp[m - 1][n - 1]
```
```Golang []
func isMatch(s string, p string) bool {
    if len(p)==0{
        if len(s)==0{
            return true
        }else{
            return false
        }
    }


    if len(s)==0 && len(p)==1{
        return false
    }

    m := len(s)+1
    n := len(p)+1

    dp := make([][]bool,m)
    for i:=0;i<m;i++{
        dp[i] = make([]bool,n)
    }

    dp[0][0] = true

    // 初始化第一行
    for j:=2;j<n;j++{
        if p[j-1]=='*'{
            dp[0][j] = dp[0][j-2]
        }
    }

    for r:=1;r<m;r++{
        i := r-1
        for c:=1;c<n;c++{
            j := c-1
            if s[i]==p[j] || p[j]=='.'{
                dp[r][c] = dp[r-1][c-1]
            }else if p[j]=='*'{
                if p[j-1]==s[i] || p[j-1]=='.'{
                    dp[r][c] = dp[r-1][c] || dp[r][c-2]
                }else{
                    dp[r][c] = dp[r][c-2]            
                }
            }else{
                dp[r][c] = false
            }
        }
    }
    return dp[m-1][n-1]
}
```
