### 1、思路
**字典树**

字典树，顾名思义，是关于“字典”的一棵树。即：它是对于字典的一种存储方式（所以是一种数据结构而不是算法）。这个词典中的每个“单词”就是从根节点出发一直到某一个目标节点的路径，路径中每条边的字母连起来就是一个单词。

![image-20211118195003075.png](https://pic.leetcode-cn.com/1644550286-wivrZG-image-20211118195003075.png)


标橙色的节点是“目标节点“，即根节点到这个目标节点的路径上的所有字母构成了一个单词。

**作用：**

-   1、维护字符串集合 **（字典）**
-   2、向字符串集合中插入字符串 **（建树）**
-   3、查询字符串集合中是否有某个字符串 **（查询）**
-   4、查询字符串集合中是否有某个字符串的前缀 **（查询）**

**具体操作：**

**定义字典树节点**

```c
 struct Node {
     bool is_end;    // 表示是否存在以这个点为结尾的单词
     Node *son[26];  // 26个小写字母子结点
     Node() {        // 初始化
         is_end = false;
         for (int i = 0; i < 26; i ++ )   
             son[i] = NULL;
     }
 }*root;
```

向字典树中插入一个单词`word`

从根结点出发，沿着字符串的字符一直往下走，若某一字符不存在，则直接把它创建出来，继续走下去，走完了整个单词，标记最后的位置的`is_end = true`。

```c
 void insert(string word) {
     auto p = root;
     for (auto c: word) {
         int u = c - 'a';
         if (!p->son[u]) p->son[u] = new Node();
         p = p->son[u];
     }
     p->is_end = true;
 }
```

查找字典树中是否存在单词`word`

从根结点出发，沿着字符串的字符一直往下走，若某一字符不存在，则直接`return false`，当很顺利走到最后的位置的时候，判断最后一个位置的`is_end`即可。

```c
 bool search(string word) {
     auto p = root;
     for (auto c: word) {
         int u = c - 'a';
         if (!p->son[u]) return false;
         p = p->son[u];
     }
     return p->is_end;
 }
```

查找字典树中是否有以`prefix`为前缀的单词

从根结点出发，沿着字符串的字符一直往下走，若某一字符不存在，则直接`return false`，如果顺利走到最后一个位置，则返回`true`。

```c
 bool startsWith(string word) {
     auto p = root;
     for (auto c: word) {
         int u = c - 'a';
         if (!p->son[u]) return false;
         p = p->son[u];
     }
     return true;
 }
```

**时间复杂度分析：** O(n)，`n`表示单词操作字符串长度。
### 2、c++代码

```c
 class Trie {
 public:
     struct Node{
         bool is_end;
         Node *son[26];
         Node(){
             is_end = false;
             for(int i = 0; i < 26; i++)
                 son[i] = NULL;
         }
     }*root;
     Trie() {
         root = new Node();
     }
     
     void insert(string word) {
         auto p = root;
         for(auto c : word){
             int u = c - 'a';
             if(!p->son[u]) p->son[u] = new Node();
             p = p->son[u];
         }
         p->is_end = true;
     }
     
     bool search(string word) {
         auto p = root;
         for(auto c : word){
             int u = c - 'a';
             if(!p->son[u]) return false;
             p = p->son[u];
         }
         return p->is_end;
     }
     
     bool startsWith(string prefix) {
         auto p = root;
         for(auto c : prefix){
             int u = c -'a';
             if(!p->son[u]) return false;
             p = p->son[u];
         }
         return true;
     }
 };
 ​
 /**
  * Your Trie object will be instantiated and called as such:
  * Trie* obj = new Trie();
  * obj->insert(word);
  * bool param_2 = obj->search(word);
  * bool param_3 = obj->startsWith(prefix);
  */
```
### 3、Java代码
```java
class Trie {

    Node root;
    public Trie() {
        root = new Node();
    }

    public void insert(String word) {
        Node p = root;
        for(int i = 0;i < word.length();i ++)
        {
            int u = word.charAt(i) - 'a';
            if(p.son[u] == null) p.son[u] = new Node();
            p = p.son[u]; 
        }
        p.is_end = true;
    }

    public boolean search(String word) {
        Node p = root;
        for(int i = 0;i < word.length();i ++)
        {
            int u = word.charAt(i) - 'a';
            if(p.son[u] == null) return false;
            p = p.son[u]; 
        }
        return p.is_end;
    }

    public boolean startsWith(String prefix) {
        Node p = root;
        for(int i = 0;i < prefix.length();i ++)
        {
            int u = prefix.charAt(i) - 'a';
            if(p.son[u] == null) return false;
            p = p.son[u]; 
        }
        return true;
    }
}
class Node 
{
    boolean is_end;
    Node[] son = new Node[26];
    Node()
    {
        is_end = false;
        for(int i = 0;i < 26;i ++)
            son[i] = null;
    } 
}
/**
 * Your Trie object will be instantiated and called as such:
 * Trie obj = new Trie();
 * obj.insert(word);
 * boolean param_2 = obj.search(word);
 * boolean param_3 = obj.startsWith(prefix);
 */

···