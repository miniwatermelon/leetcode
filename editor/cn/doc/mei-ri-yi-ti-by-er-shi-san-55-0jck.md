### 解题思路
比较笨的方法~
### 代码

```java
class Solution {
    public boolean canJump(int[] nums) {
        int len = nums.length;
        int[] result = new int[len];
        result[0] = 1;
        for (int i = 0; i < result.length; i++) {
            if (result[i]==1){
                if (nums[i] >= len-1-i){
                    result[len-1] = 1;
                    break;
                }else{
                    for (int j = 1; j <= nums[i]; j++) {
                        result[i+j] = 1;
                    }
                }
            }else {
                continue;
            }
        }
        return result[nums.length-1]==1;
    }
}
```