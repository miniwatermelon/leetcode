解法一（递归解法）：
```
class Solution {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> retList = new ArrayList<>();
        if (root == null){
             return retList;   
        }

        retList.add(root.val);

        List<Integer> leftList = preorderTraversal(root.left);
        retList.addAll(leftList);

        List<Integer> rightList = preorderTraversal(root.right);
        retList.addAll(rightList);
        
        return retList;
    }
}
```
解法二（非递归解法）：
```
class Solution {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> ret = new ArrayList<>();
        //1、借助栈来非递归解题
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        //4、此时发现右树的遍历没有循环，故在外添加一个循环
        //为防止从空栈中提取元素，添加循环条件栈不为空
        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                //2、循环遍历左树，依次打印根节点，直到cur为空
                stack.push(cur);
                ret.add(cur.val);
                cur = cur.left;
            }
            //3、当左树为空时，弹出栈顶元素并接收，以此遍历右树
            TreeNode top = stack.pop();
            cur = top.right;
        }
        return ret;
    }
}
```
