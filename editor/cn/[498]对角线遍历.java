//给你一个大小为 m x n 的矩阵 mat ，请以对角线遍历的顺序，用一个数组返回这个矩阵中的所有元素。 
//
// 
//
// 示例 1： 
//
// 
//输入：mat = [[1,2,3],[4,5,6],[7,8,9]]
//输出：[1,2,4,7,5,3,6,8,9]
// 
//
// 示例 2： 
//
// 
//输入：mat = [[1,2],[3,4]]
//输出：[1,2,3,4]
// 
//
// 
//
// 提示： 
//
// 
// m == mat.length 
// n == mat[i].length 
// 1 <= m, n <= 10⁴ 
// 1 <= m * n <= 10⁴ 
// -10⁵ <= mat[i][j] <= 10⁵ 
// 
// Related Topics 数组 矩阵 模拟 👍 336 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution498 {
    public int[] findDiagonalOrder(int[][] mat) {
        int rowLen = mat.length;
        int colLen = mat[0].length;
        int size = rowLen * colLen;
        int row = 0, col = 0;
        int[] arr = new int[size];
        int index = 0;
        // 行遍历
        while(row < rowLen) {
            int tempRow = row, tempCol = col;
            for(int i = tempRow; i >=0; i--) {
                arr[index] = mat[tempCol][tempRow];
                tempRow--;
                tempCol++;
                index++;
            }
            if(row == rowLen - 1) break;

            row++;
        }
        col++;
        // 列遍历
        while(col < colLen) {
            int tempRow = row, tempCol = col;
            for(int i = tempCol; i <colLen; i++) {
                arr[index] = mat[tempCol][tempRow];
                tempRow--;
                tempCol++;
                index++;
            }
            if(col == colLen - 1) break;
            col++;
        }
        return arr;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
