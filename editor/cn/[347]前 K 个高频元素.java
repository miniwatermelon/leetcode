//给你一个整数数组 nums 和一个整数 k ，请你返回其中出现频率前 k 高的元素。你可以按 任意顺序 返回答案。 
//
// 
//
// 示例 1: 
//
// 
//输入: nums = [1,1,1,2,2,3], k = 2
//输出: [1,2]
// 
//
// 示例 2: 
//
// 
//输入: nums = [1], k = 1
//输出: [1] 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 105 
// k 的取值范围是 [1, 数组中不相同的元素的个数] 
// 题目数据保证答案唯一，换句话说，数组中前 k 个高频元素的集合是唯一的 
// 
//
// 
//
// 进阶：你所设计算法的时间复杂度 必须 优于 O(n log n) ，其中 n 是数组大小。 
// Related Topics 数组 哈希表 分治 桶排序 计数 快速选择 排序 堆（优先队列） 
// 👍 1045 👎 0


import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution347 {
    /**
     * 解题思路：
     * 1.将各个元素和元素总数放入map中
     * 2.构建大顶堆
     * 3.输入大顶堆的前k个元素
     *
     * 解答成功:
     * 			执行耗时:11 ms,击败了94.48% 的Java用户
     * 			内存消耗:43.7 MB,击败了29.75% 的Java用户
     * @param nums
     * @param k
     * @return
     */
    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Node> map = new HashMap<>();
        for (int num : nums) {
            Node node = map.getOrDefault(num, new Node(num));
            node.addSum();
            map.put(num, node);
        }

        PriorityQueue<Node> queue = new PriorityQueue<>(map.size(), (o1, o2) -> o2.sum - o1.sum);
        for (Node node : map.values()) {
            queue.add(node);
        }

        int[] res = new int[k];
        for (int i = 0; i < k; i++) {
            res[i] = queue.poll().val;
        }

        return res;
    }


}

class Node {
    int val;
    int sum;

    public Node(int val) {
        this.val = val;
    }

    public void addSum() {
        sum++;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
