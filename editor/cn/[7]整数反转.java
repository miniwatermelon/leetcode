//给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。 
//
// 如果反转后整数超过 32 位的有符号整数的范围 [−2³¹, 231 − 1] ，就返回 0。 
//假设环境不允许存储 64 位整数（有符号或无符号）。
//
// 
//
// 示例 1： 
//
// 
//输入：x = 123
//输出：321
// 
//
// 示例 2： 
//
// 
//输入：x = -123
//输出：-321
// 
//
// 示例 3： 
//
// 
//输入：x = 120
//输出：21
// 
//
// 示例 4： 
//
// 
//输入：x = 0
//输出：0
// 
//
// 
//
// 提示： 
//
// 
// -2³¹ <= x <= 2³¹ - 1 
// 
// Related Topics 数学 👍 3538 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution7 {
    /**
     * 解题思路：
     * 1.源数字取模，输出数字*10+模的值
     * 2.唯一要注意的地方就是边界问题
     * <p>
     * 执行耗时:0 ms,击败了100.00% 的Java用户
     * 内存消耗:38.6 MB,击败了73.74% 的Java用户
     */
    public int reverse(int x) {
        int res = 0;
        int tmp = 10;
        while(x != 0) {
            int num = x%tmp;
            if(x>0 && (res > Integer.MAX_VALUE/tmp || (res == Integer.MAX_VALUE/tmp && Integer.MAX_VALUE - res*tmp <= num))) {
                return 0;
            }
            if(x<0 && (res < Integer.MIN_VALUE/tmp ||  (res == Integer.MAX_VALUE/tmp && Integer.MIN_VALUE - res*tmp >= num))) {
                return 0;
            }
            res = res*tmp + num;
            x /= tmp;
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(new Solution7().reverse(1463847412));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
