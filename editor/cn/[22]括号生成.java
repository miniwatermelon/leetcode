//数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。 
//
// 
//
// 示例 1： 
//
// 
//输入：n = 3
//输出：["((()))","(()())","(())()","()(())","()()()"]
// 
//
// 示例 2： 
//
// 
//输入：n = 1
//输出：["()"]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= n <= 8 
// 
// Related Topics 字符串 动态规划 回溯 
// 👍 2382 👎 0


import java.util.ArrayList;
import java.util.List;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution22 {
    String left = "(", right = ")";

    /**
     * 解题思路
     * 1.如果当前是 (  则下一个值可能是 ( 和 ）
     * 2.如果当前是 ) 则下一个值可以是 （ ， 如果 左括号总数大于右括号， 则下一个值可以是 ) 直到两个括号数相等
     * 3.循环完成后补齐 )
     *
     * 解答成功:
     * 			执行耗时:1 ms,击败了74.82% 的Java用户
     * 			内存消耗:41.2 MB,击败了23.92% 的Java用户
     * @param n
     * @return
     */
    public List<String> generateParenthesis(int n) {
        List<String> result = new ArrayList<>();
        if(n==0) return result;
        generateParenthesis(1, 0, n, result, left, left);
        return result;
    }

    /**
     * 递归调用生成
     * @param l 左括号的数量
     * @param r 右括号的数量
     * @param n 总长度
     * @param result 结果集
     * @param res 当前结果
     * @param str 下一个括号值
     */
    private void generateParenthesis(int l, int r, int n, List<String> result, String res, String str) {
        if(l < n && r < n) {
            if(right.equals(str)) {
                if(l>r) {
                    generateParenthesis(l, r+1, n, result, res + right, right);
                }
                generateParenthesis(l+1, r, n, result, res + left, left);
            } else {
                generateParenthesis(l+1, r, n, result, res + left, left);
                generateParenthesis(l, r+1, n, result, res + right, right);
            }
        } else {
            StringBuilder resBuilder = new StringBuilder(res);
            for(int k = r; k<n; k++) {
                resBuilder.append(right);
            }
            result.add(resBuilder.toString());
        }
    }

    public static void main(String[] args) {
        System.out.println(new Solution22().generateParenthesis(2));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
