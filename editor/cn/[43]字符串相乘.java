//给定两个以字符串形式表示的非负整数 num1 和 num2，返回 num1 和 num2 的乘积，它们的乘积也表示为字符串形式。 
//
// 注意：不能使用任何内置的 BigInteger 库或直接将输入转换为整数。 
//
// 
//
// 示例 1: 
//
// 
//输入: num1 = "2", num2 = "3"
//输出: "6" 
//
// 示例 2: 
//
// 
//输入: num1 = "123", num2 = "456"
//输出: "56088" 
//
// 
//
// 提示： 
//
// 
// 1 <= num1.length, num2.length <= 200 
// num1 和 num2 只能由数字组成。 
// num1 和 num2 都不包含任何前导零，除了数字0本身。 
// 
// Related Topics 数学 字符串 模拟 
// 👍 846 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
import java.math.BigDecimal;
class Solution43 {
    public String multiply(String num1, String num2) {

        char[] num2CharArray = num2.toCharArray();
        char[] num1CharArray = num1.toCharArray();

        BigDecimal result = new BigDecimal(0);
        BigDecimal temp = new BigDecimal(1);
        for (int i = num2CharArray.length - 1; i >= 0; i--) {
            BigDecimal num2BigDecimal = temp.multiply(new BigDecimal(num2CharArray[i] - '0'));

            result = result.add(mutiply(num2BigDecimal, num1CharArray));
            temp = temp.multiply(new BigDecimal(10));
        }

        return result.toString();
    }

    private static BigDecimal mutiply(BigDecimal num2BigDecimal, char[] num1CharArray) {
        BigDecimal result = new BigDecimal(0);
        BigDecimal temp = new BigDecimal(1);
        for (int j = num1CharArray.length - 1; j >= 0; j--) {
            BigDecimal num1BigDecimal = temp.multiply(new BigDecimal(num1CharArray[j] - '0'));
            result = result.add(num1BigDecimal.multiply(num2BigDecimal));
            temp = temp.multiply(new BigDecimal(10));
        }
        return result;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
 