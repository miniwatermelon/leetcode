//给你一个含 n 个整数的数组 nums ，其中 nums[i] 在区间 [1, n] 内。请你找出所有在 [1, n] 范围内但没有出现在 nums 中的数
//字，并以数组的形式返回结果。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [4,3,2,7,8,2,3,1]
//输出：[5,6]
// 
//
// 示例 2： 
//
// 
//输入：nums = [1,1]
//输出：[2]
// 
//
// 
//
// 提示： 
//
// 
// n == nums.length 
// 1 <= n <= 105 
// 1 <= nums[i] <= n 
// 
//
// 进阶：你能在不使用额外空间且时间复杂度为 O(n) 的情况下解决这个问题吗? 你可以假定返回的数组不算在额外空间内。 
// Related Topics 数组 哈希表 
// 👍 890 👎 0


import java.util.*;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution448 {
    /**
     * 解题思路：
     * 1. 元素数组映射为hashset
     * 2. 匹配 n是否在hashset中存在，不存在则记录为集合
     * 解答成功:
     * 			执行耗时:13 ms,击败了21.59% 的Java用户
     * 			内存消耗:49.6 MB,击败了5.03% 的Java用户
     *
     * @param nums
     * @return
     */
    public List<Integer> findDisappearedNumbers(int[] nums) {
        Set<Integer> set = new HashSet<>(nums.length);
        for (int num : nums) {
            set.add(num);
        }

        List<Integer> result = new ArrayList<>();
        for(int i=1; i<=nums.length; i++) {
            if(!set.contains(i)) {
                result.add(i);
            }
        }
        return result;
    }

    /**
     * 解题思路：
     * 1. 数组元素值不超过数组最大下标， 因此数组元素值-1 % 数组长度 = 数组元素值-1， 将其标记为需要处理的下标
     * 2. 将数组下标对应的值 + 数组长度，使其超过数组长度值
     * 3. 在循环本身的数组，如果判断有值小于数组长度，说明在之前的循环中没有处理到这个下标，也就是没有对应的元素值可以获取到这个下标，那么下标+1 即为未出现的数值。
     * @param nums
     * @return
     */
    public List<Integer> findDisappearedNumbersAnswer(int[] nums) {
        List<Integer> list=new ArrayList<>();
        int n=nums.length;
        for(int i=0;i<nums.length;i++){
            int j=(nums[i]-1)%n;//加上n之后 依然是对应位置 取模还原他本来的值
            nums[j]+=n;//对应位置加n
        }
        for(int i=0;i<nums.length;i++){
            if(nums[i]<=n)list.add(i+1);//数组下标从0开始 记得加一
        }
        return list;
    }


    public static void main(String[] args) {
        System.out.println(new Solution448().findDisappearedNumbersAnswer(new int[]{4, 3, 2, 7, 8, 2, 3, 1}));
        System.out.println(new Solution448().findDisappearedNumbers(new int[]{1,1}));
    }
}
//leetcode submit region end(Prohibit modification and deletion)


