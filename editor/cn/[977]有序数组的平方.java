//给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。 
//
// 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [-4,-1,0,3,10]
//输出：[0,1,9,16,100]
//解释：平方后，数组变为 [16,1,0,9,100]
//排序后，数组变为 [0,1,9,16,100] 
//
// 示例 2： 
//
// 
//输入：nums = [-7,-3,2,3,11]
//输出：[4,9,9,49,121]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 10⁴ 
// -10⁴ <= nums[i] <= 10⁴ 
// nums 已按 非递减顺序 排序 
// 
//
// 
//
// 进阶： 
//
// 
// 请你设计时间复杂度为 O(n) 的算法解决本问题 
// 
//
// Related Topics 数组 双指针 排序 👍 1011 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution977 {
    public int[] sortedSquares(int[] nums) {
        int[] res = new int[nums.length];
        int resIdx = res.length - 1;
        int leftIdx = 0, rightIdx = nums.length - 1;
        while(leftIdx <= rightIdx) {
            int leftVal = nums[leftIdx] * nums[leftIdx];
            int rightVal = nums[rightIdx] * nums[rightIdx];
            if(leftVal < rightVal) {
                res[resIdx] = rightVal;
                rightIdx--;
            }else {
                res[resIdx] = leftVal;
                leftIdx++;
            }
            resIdx--;
        }
        return res;
    }

    public int[] sortedSquares1(int[] nums) {

        nums[0] = nums[0]*nums[0];
        if(nums.length == 1) {
            return nums;
        }

        int minIdx = 0;
        for (int i = 1; i < nums.length; i++) {
            nums[i] = nums[i]*nums[i];
            if(nums[i-1] > nums[i]) {
                minIdx=i;
            }
        }

        int[] res = new int[nums.length];
        res[0] = nums[minIdx];
        int idx = 1;
        int leftIdx = minIdx - 1;
        int rightIdx = minIdx + 1;
        while(leftIdx >=0 && rightIdx<= nums.length-1) {
            if(nums[leftIdx] < nums[rightIdx]) {
                res[idx] = nums[leftIdx];
                idx++;
                leftIdx--;
            }else {
                res[idx] = nums[rightIdx];
                idx++;
                rightIdx++;
            }
        }
        while(leftIdx >=0) {
            res[idx] = nums[leftIdx];
            idx++;
            leftIdx--;
        }
        while(rightIdx<= nums.length-1) {
            res[idx] = nums[rightIdx];
            idx++;
            rightIdx++;
        }
        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
