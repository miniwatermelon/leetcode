//将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。 
//
// 比如输入字符串为 "PAYPALISHIRING" 行数为 3 时，排列如下： 
//
// 
//P   A   H   N
//A P L S I I G
//Y   I   R 
//
// 之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："PAHNAPLSIIGYIR"。 
//
// 请你实现这个将字符串进行指定行数变换的函数： 
//
// 
//string convert(string s, int numRows); 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "PAYPALISHIRING", numRows = 3
//输出："PAHNAPLSIIGYIR"
// 
//示例 2：
//
// 
//输入：s = "PAYPALISHIRING", numRows = 4
//输出："PINALSIGYAHRPI"
//解释：
//P     I    N
//A   L S  I G
//Y A   H R
//P     I
// 
//
// 示例 3： 
//
// 
//输入：s = "A", numRows = 1
//输出："A"
// 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 1000 
// s 由英文字母（小写和大写）、',' 和 '.' 组成 
// 1 <= numRows <= 1000 
// 
// Related Topics 字符串 👍 1701 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution6 {
    /**
     * 解题思路：
     * 1.根据Z字字符串生成的规律，将字符串先分段，后分组，然后遍历处理
     * 2.分段的依据是：numRows>1的情况， 段segment = numRows + (numRows - 2)，这样可以保证一段数据中正好是一串Z形字符串
     * 3.分组的依据是：根据分段的长度来分组，即表示有多少组这种Z形字符串的段
     * 4.每一段中还需要考虑Z形字符串的索引位置，根据规律可知，一段数据中，同一行位置的数据可能出现在两处，一处是包含在nunRows垂直串，一处是包含Z串，如果是Z串，则位置是第几个numRows位置的segment-2
     *<p>
     * 执行耗时:2 ms,击败了98.60% 的Java用户
     * 内存消耗:41.5 MB,击败了88.43% 的Java用户
     */
    public String convert1(String s, int numRows) {
        int segment = (numRows == 1) ? numRows : numRows + (numRows - 2);
        StringBuilder result = new StringBuilder();
        int group = (s.length() % segment == 0) ? s.length() / segment : s.length() / segment + 1;
        int load = segment;
        for (int num = 0; num < numRows; num++) {
            for (int i = 0; i < group; i++) {
                int idx = num + (i * segment);
                if(idx < s.length()) {
                    result.append(s.charAt(idx));
                }
                if(load == segment || load == 0) continue;
                idx += load;
                if(idx < s.length() && idx < (i+1)*segment) {
                    result.append(s.charAt(idx));
                }
            }
            load -= 2;
        }
        return result.toString();
    }

    /**
     * 解题思路：
     * 1.创建一个StringBuilder数组
     * 2.遍历字符串，获取到应该放到第几个stringBuilder数组的位置。默认是顺序放下，当碰到Z形顶点位置时，调转放置数组的方向
     * 3.组合stringbuilder数组的值
     *<p>
     * 执行耗时:4 ms,击败了84.21% 的Java用户
     * 内存消耗:42.2 MB,击败了29.62% 的Java用户
     */
    public String convert(String s, int numRows) {
        if(numRows <=1) return s;

        StringBuilder[] builders = new StringBuilder[numRows];
        for (int i = 0; i < builders.length; i++) {
            builders[i] = new StringBuilder();

        }
        int i=0,flag=-1;
        for (int idx = 0; idx < s.length(); idx++) {
            builders[i].append(s.charAt(idx));
            if(i==0 || i == numRows-1) {
                flag *= -1;
            }
            i += flag;
        }
        StringBuilder res = new StringBuilder();
        for (StringBuilder builder : builders) {
            res.append(builder.toString());
        }
        return res.toString();
    }
    public static void main(String[] args) {
        System.out.println(new Solution6().convert("PAYPALISHIRING", 3));
        System.out.println(new Solution6().convert("PAYPALISHIRING", 5));
        System.out.println(new Solution6().convert("A", 1));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
