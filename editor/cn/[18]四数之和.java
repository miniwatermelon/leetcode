//给你一个由 n 个整数组成的数组 nums ，和一个目标值 target 。请你找出并返回满足下述全部条件且不重复的四元组 [nums[a], nums[
//b], nums[c], nums[d]] （若两个四元组元素一一对应，则认为两个四元组重复）： 
//
// 
// 0 <= a, b, c, d < n 
// a、b、c 和 d 互不相同 
// nums[a] + nums[b] + nums[c] + nums[d] == target 
// 
//
// 你可以按 任意顺序 返回答案 。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,0,-1,0,-2,2], target = 0
//输出：[[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]
// 
//
// 示例 2： 
//
// 
//输入：nums = [2,2,2,2,2], target = 8
//输出：[[2,2,2,2]]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 200 
// -10⁹ <= nums[i] <= 10⁹ 
// -10⁹ <= target <= 10⁹ 
// 
//
// Related Topics 数组 双指针 排序 👍 1945 👎 0


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution18{
    public List<List<Integer>> fourSum(int[] nums, int target) {
        if(nums.length < 4) {
            return new ArrayList<>();
        }
        Arrays.sort(nums);

        List<List<Integer>> resultList = new ArrayList<>();

        for (int k = 0; k < nums.length; k++) {
            if(target > 0 && nums[k] > target) {
                return resultList;
            }
            if(k > 0 && nums[k] == nums[k-1]) {
                continue;
            }
            long a = nums[k];
            for (int i = k+1; i < nums.length; i++) {
                if(i > (k+1) && nums[i] == nums[i-1]) {
                    continue;
                }
                long b = nums[i];
                int left = i+1;
                int right = nums.length - 1;
                while(left < right) {
                    long c = nums[left];
                    long d = nums[right];
                    if(a+b+c+d == target) {
                        resultList.add(Arrays.asList(nums[k], nums[i], nums[left], nums[right]));

                        do{
                            left++;
                        } while(left<right && nums[left] == nums[left-1]);

                        do{
                            right--;
                        } while(left<right && nums[right] == nums[right+1]);
                    } else if(a+b+c+d > target) {
                        right -- ;
                    } else {
                        left++;
                    }

                }
            }
        }
        return resultList;
    }

    public static void main(String[] args) {
//        System.out.println(new Solution18().fourSum(new int[]{-1, 0, 1, 2, -1, -4}, -1));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
